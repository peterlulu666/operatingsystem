#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>


#define MAX_FOLDER_ARRAY_SIZE 1024

#define MAX_FILE_ARRAY_SIZE 1024

#define MAX_NAME_SIZE 2048

// Create the folder_array to store directory names
char folder_array[MAX_FOLDER_ARRAY_SIZE][MAX_NAME_SIZE];
// Count how many elements we will add to the folder_array in the current iteration
int folder_length = 0;

// Create the file_array to store file names
char file_array[MAX_FILE_ARRAY_SIZE][MAX_NAME_SIZE];
// Count how many elements we will add to the file_array in the current iteration
int file_length = 0;

// Create file_disk_size_array to store file size
int file_disk_size_array[MAX_FILE_ARRAY_SIZE];

// Create file_time_and_date_array to store file time and date
char file_time_and_date_array[MAX_FILE_ARRAY_SIZE][MAX_NAME_SIZE];

// Create file_permission_array to store file permission
char file_permission_array[MAX_FILE_ARRAY_SIZE][MAX_NAME_SIZE];

// typedef struct
// those info are sorted together
typedef struct fileInfo {
    char file_name[256];
    int file_size;
    char file_permission[256];
    int file_year;
    int file_month;
    int file_day;
    int file_hour;
    int file_minute;
    int file_second;
} FILEINFO;

// Print from January to December
char month_array[12][256] = {"January",
                             "February",
                             "March",
                             "April",
                             "May",
                             "June",
                             "July",
                             "August",
                             "September",
                             "October",
                             "November",
                             "December"};

int cmp_date_time(const void *a, const void *b);

int cmp_size(const void *a, const void *b);


int main() {
    pid_t child;
    DIR *d;
    struct dirent *de;
    int i, c, k;
    char s[256], cmd[256];
    time_t t;

    while (true) {
        printf("+-------------------Welcome-------------------+ \n");
        printf("|                                             | \n");
        printf("|          Welcome to myshell program         | \n");
        printf("|                                             | \n");
        printf("+---------------------------------------------+ \n");

        t = time(NULL);
        printf("====================TIME=======================\n");
        printf("           %s                                  \n", ctime(&t));
//        printf("Time: %s\n", ctime(&t));


        getcwd(s, 200);
        printf("===========CURRENT WORKING DIRECTORY===========\n");
        printf("%s                                             \n\n", s);
//        printf("\nCurrent Directory: %s \n", s);

        // Print directories
        // We will store the director names to folder_array and
        // print the folder_array elements from index 0 to folder_length
        printf("=================DIRECTORIES===================\n");

        // Set the folder_length to 0
        // In every iteration, we will get started copying
        // directory names to folder_array at the index 0
        folder_length = 0;

        // Store directory names to folder_array
        d = opendir(".");
        c = 0;
        struct stat folder_buffer;
        // File path
        char folder_path[256];
        // File name
        char folder_name[256];
        // Traverse directory
        while ((de = readdir(d))) {
//            // I am experimenting if S_ISDIR will check if it is the directory
//            // It is working, but I prefer to use the code provided by professor
//            strcpy(folder_path, "./");
//            strcpy(folder_name, de->d_name);
//            strcat(folder_path, folder_name);
//            if (!stat(folder_path, &folder_buffer)) {
//                // Check if it is the directory
//                if (S_ISDIR(folder_buffer.st_mode)) {
//                    // If it is the directory, append the directory name to the folder_array
//                    strcpy(folder_array[folder_length], de->d_name);
//                    folder_length++;
//                }
//            }

            // Check if it is the directory
            if ((de->d_type) & DT_DIR) {
                // If it is the directory, append the directory name to the folder_array
                strcpy(folder_array[folder_length], de->d_name);
                folder_length++;
            }
        }

        folder_length = folder_length;
        closedir(d);

        // If the folder_array is empty, it will not print the directory name
        if (folder_length == 0) {
            printf("There is no subdirectory in the current directory. \n");
        }

        // If the folder_array is not empty, it will print the directory name
        int folder_index = 0;
        int folder_n_or_p = 0;
        // If you do not initialize the folder_quit, it will go crazy
        int folder_quit = 0;
        while (folder_index < folder_length) {
            if (folder_quit == 'q' || folder_quit == 'Q') {
                break;
            }

            printf("           %d Directories: %s \n", folder_index, folder_array[folder_index]);
            folder_index++;
            // You can only show 5 lines of file names in one page
            // You will need to implement Prev, and Next Operations so that the menu fits
            // on one screen
            if (folder_index % 5 == 0) {
                printf("+---------------Directory menu----------------+ \n");
                printf("|                                             | \n");
                printf("|      Hit N for Next or hit P for prev       | \n");
                printf("|        If you do not want to display        | \n");
                printf("|    additional directories, press q to quit  | \n");
                printf("|           displaying directories            | \n");
                printf("+---------------------------------------------+ \n");
                printf("Enter your choice: ");
//                printf("Hit N for Next or hit P for prev. "
//                       "If you do not want to display additional directories, "
//                       "press q to quit displaying directories \n");
                folder_n_or_p = getchar();
                // Use getchar() to eat the newline
                getchar();
                folder_quit = folder_n_or_p;
                if (folder_quit == 'q' || folder_quit == 'Q') {
                    continue;
                }
                while (true) {
                    if (folder_n_or_p == 'n' || folder_n_or_p == 'N') {
                        break;
                    }
                    if (folder_n_or_p == 'p' || folder_n_or_p == 'P') {
                        // User should not press p in the first page
                        // printf("           %d Directories: %s \n", folder_index,
                        // folder_array[folder_index]); is before folder_index++;
                        // Let's say that you are in the page 2,
                        // the file_index is 10, but the last file it will print is at file_index 9
                        // So file_index - 10 is 0, in the next iteration,
                        // it will get started printing at file_index 0, which is the first page
                        // It is the same for page 3, 4, 5, 6...
                        folder_index = folder_index - 10;

                        // Of course a user may type “P” ten times in succession, so
                        // you shouldn’t try to move the “window” of files
                        // you are showing to “before” the first

                        // If the user press p in the first page
                        // it will tell the user that this is the first page and
                        // it will display files in the first page
                        // so if the file_index is < 0, we need to set it to 0,
                        // in the next iteration, it will get started
                        // printing at file_index 0, which is the first page
                        if (folder_index <= 0) {
                            printf("+--------------Oops, that's wrong-------------+ \n");
                            printf("|                                             | \n");
                            printf("|     It would not display any page before    | \n");
                            printf("|                the first page               | \n");
                            printf("+---------------------------------------------+ \n");
//                            printf("You reach the first page. There is no directories before the directory 0. "
//                                   "So it would not display any page before the first page. \n");
                            folder_index = 0;
                        }
                        break;
                    }

                    // If the user did not choose N or P,
                    // let the user know that they need to choose N or P
                    printf("+--------------Oops, that's wrong-------------+ \n");
                    printf("|                                             | \n");
                    printf("|        You should choose N or P             | \n");
                    printf("|      Hit N for Next or hit P for prev       | \n");
                    printf("|        If you do not want to display        | \n");
                    printf("|    additional directories, press q to quit  | \n");
                    printf("|           displaying directories            | \n");
                    printf("+---------------------------------------------+ \n");
                    printf("Enter your choice: ");
//                    printf("You should choose N or P. "
//                           "Hit N for Next or hit P for prev. "
//                           "If you do not want to display additional directories, "
//                           "press q to quit displaying directories \n");
                    folder_n_or_p = getchar();
                    // Use getchar() to eat the newline
                    getchar();
                    folder_quit = folder_n_or_p;
                    if (folder_quit == 'q' || folder_quit == 'Q') {
                        break;
                    }
                }
            }
        }

        printf("-----------------------------------------------\n");

        // Print files
        // We will store the file names to file_array and
        // print the file_array elements from index 0 to file_length
        printf("====================FILES======================\n");

        // Set the file_length to 0
        // In every iteration, we will get started copying
        // file names to file_array at the index 0
        // If we use change directory, move file to directory or remove file,
        // the number of files will change, let's say that there is 20 files, we remove 10 files,
        // in the next iteration, we will only add 10 file names to
        // the file_array from index 0 to index 9.
        // However, there are still file names in the file_array
        // from index 10 to index 19 and we do not need them.
        // Do we need to delete them? No.
        // When printing the file_array, we only print elements from index 0 to
        // array length, which is 10.
        // When we use edit or run operation, we will check if the user enters a number that is
        // greater than 0 and less than array length, which is 10.
        file_length = 0;

        // Store the file names to the file_array
        d = opendir(".");
        c = 0;
        FILE *fp;
        // File size
        int len = 0;
        struct stat buffer;
        // File path
        char file_path[256];
        // File name
        char file_name[256];
        // File time and date
        char file_time_and_date_str[256] = "";
        // File permission
        char file_permission[256] = "";
        // Traverse directory
        while ((de = readdir(d))) {
            // Check if it is the regular file
            // Use S_ISREG(buffer.st_mode) to check if it is the regular file
            // if (!stat(file_path, &buffer)) {
            //      if (S_ISREG(buffer.st_mode)){
            //          write your code
            //      }
            // }
            if (((de->d_type) & DT_REG)) {
                // If it is the regular file, append the file name to file_array
                strcpy(file_array[file_length], de->d_name);

                // Show additional file information, in addition to file name, show file size,
                // time and date of creation, R/W/X (read/write/execute permissions),
                // and similar information

//                // It is working, but there is a better way to do it
//                // file size
//                /*This code was taken from the website
//                 * Source code website:
//                 * https://www.tutorialspoint.com/c_standard_library/c_function_ftell.htm
//                 * Source code:
//                 *  #include <stdio.h>
//
//                    int main () {
//                       FILE *fp;
//                       int len;
//
//                       fp = fopen("file.txt", "r");
//                       if( fp == NULL )  {
//                          perror ("Error opening file");
//                          return(-1);
//                       }
//                       fseek(fp, 0, SEEK_END);
//
//                       len = ftell(fp);
//                       fclose(fp);
//
//                       printf("Total size of file.txt = %d bytes\n", len);
//
//                       return(0);
//                    }
//                 *
//                 * What I have learned:
//                 * I learned how to use fopen() function, fseek() function and ftell function to get the file size
//                 * */
//                fp = fopen(de->d_name, "r");
//                if (fp == NULL) {
//                    perror("Error opening file");
//                    return (-1);
//                }
//                fseek(fp, 0, SEEK_END);
//                len = ftell(fp);
//                fclose(fp);
//                // Append file size to the file_disk_size_array
//                file_disk_size_array[file_length] = len;


                // In order to show file size, time and date of creation, R/W/X
                // First, we need to get the size of file in bytes and
                // append the integer to the file_disk_size_array
                // Second, we need to get the time and date of creation of file and
                // append the string to the file_time_and_date_array
                // Third, we need to get the permission of file and
                // append the string to the file_permission_array
                strcpy(file_path, "./");
                strcpy(file_name, de->d_name);
                strcat(file_path, file_name);
                if (!stat(file_path, &buffer)) {
                    // If you look at the man page 2 stat, you will find struct stat field
                    // st_size: file size, in bytes
                    /*This code was taken from the website
                     * Source code website:
                     * https://stackoverflow.com/questions/8236/how-do-you-determine-the-size-of-a-file-in-c
                     * Source code:
                     *  #include <sys/stat.h>
                        off_t fsize(char *file) {
                            struct stat filestat;
                            if (stat(file, &filestat) == 0) {
                                return filestat.st_size;
                            }
                            return 0;
                        }
                     *
                     * What I have learned:
                     * I learned how to use the stat() system call to get file size
                     * */
                    // Append file size to the file_disk_size_array
                    file_disk_size_array[file_length] = buffer.st_size;

                    // If you look at the man page 2 stat, you will find the time-related fields of struct stat
                    // st_atime: Time when file data last accessed
                    // st_mtime: Time when file data last modified
                    // st_ctime: Time when file status was last changed
                    // st_birthtime: Time of file creation
                    // We need to use st_birthtime for the time and date of creation
                    /*This code was taken from the website
                     * Source code website:
                     * https://www.tutorialspoint.com/c_standard_library/c_function_strftime.htm
                     * Source code:
                           strftime(buffer,80,"%x - %I:%M%p", info);
                     *
                     * What I have learned:
                     * I learned how to use the strftime() function to format time
                     *
                     * Source code website:
                     * https://www.daniweb.com/programming/software-development/threads/25812/how-to-get-last-accessed-created-modified-file-date-and-time
                     * Source code:
                     *  #include <time.h>
                        #include <sys/types.h>
                        #include <sys/stat.h>
                        #include <stdio.h>
                        int main()
                        {
                            char filename[] = "c:\\test.txt";
                            char timeStr[ 100 ] = "";
                            struct stat buf;
                            time_t ltime;
                            char datebuf [9];
                            char timebuf [9];
                            if (!stat(filename, &buf))
                            {
                                strftime(timeStr, 100, "%d-%m-%Y %H:%M:%S", localtime( &buf.st_mtime));
                                printf("\nLast modified date and time = %s\n", timeStr);
                     *
                     * What I have learned:
                     * I learned how to use the strftime() function to format time
                     * */
                    // buffer.st_birthtime is the long type, we will convert long to string
                    // We will use strftime() to format time
                    // The specifier we will use is
                    // %x: Date representation, for example, 08/19/12
                    // %I: Hour in 12h format, for example, 05
                    // %M: Minute, for example, 55
                    // %p AM or PM designation, for example, PM
                    strftime(file_time_and_date_str, 256, "%Y-%m-%d %H:%M:%S", localtime(&(buffer.st_birthtime)));
                    // Append time_and_date_str to time_and_date_array
                    strcpy(file_time_and_date_array[file_length], file_time_and_date_str);

                    // If you look at the man page 2 stat, you will find the file mode bits
                    // S_IRUSR: read permission, owner
                    // S_IWUSR: write permission, owner
                    // S_IXUSR: execute/search permission, owner
                    // S_IRGRP: read permission, group
                    // S_IWGRP: write permission, group
                    // S_IXGRP: execute/search permission, group
                    // S_IROTH: read permission, others
                    // S_IWOTH: write permission, others
                    // S_IXOTH: execute/search permission, others
                    /*This code was taken from the website
                     * Source code website:
                     * https://stackoverflow.com/questions/10323060/printing-file-permissions-like-ls-l-using-stat2-in-c
                     * Source code:
                     *  #include <unistd.h>
                        #include <stdio.h>
                        #include <sys/stat.h>
                        #include <sys/types.h>

                        int main(int argc, char **argv)
                        {
                            if(argc != 2)
                                return 1;

                            struct stat fileStat;
                            if(stat(argv[1], &fileStat) < 0)
                                return 1;

                            printf("Information for %s\n", argv[1]);
                            printf("---------------------------\n");
                            printf("File Size: \t\t%d bytes\n", fileStat.st_size);
                            printf("Number of Links: \t%d\n", fileStat.st_nlink);
                            printf("File inode: \t\t%d\n", fileStat.st_ino);

                            printf("File Permissions: \t");
                            printf( (S_ISDIR(fileStat.st_mode)) ? "d" : "-");
                            printf( (fileStat.st_mode & S_IRUSR) ? "r" : "-");
                            printf( (fileStat.st_mode & S_IWUSR) ? "w" : "-");
                            printf( (fileStat.st_mode & S_IXUSR) ? "x" : "-");
                            printf( (fileStat.st_mode & S_IRGRP) ? "r" : "-");
                            printf( (fileStat.st_mode & S_IWGRP) ? "w" : "-");
                            printf( (fileStat.st_mode & S_IXGRP) ? "x" : "-");
                            printf( (fileStat.st_mode & S_IROTH) ? "r" : "-");
                            printf( (fileStat.st_mode & S_IWOTH) ? "w" : "-");
                            printf( (fileStat.st_mode & S_IXOTH) ? "x" : "-");
                            printf("\n\n");

                            printf("The file %s a symbolic link\n", (S_ISLNK(fileStat.st_mode)) ? "is" : "is not");

                        What I have learned:
                        I learned how to print file permission in c language
                     * */
                    strcpy(file_permission, "");
                    strcat(file_permission, (buffer.st_mode & S_IRUSR) ? "r" : "-");
                    strcat(file_permission, (buffer.st_mode & S_IWUSR) ? "w" : "-");
                    strcat(file_permission, (buffer.st_mode & S_IXUSR) ? "x" : "-");
                    strcat(file_permission, (buffer.st_mode & S_IRGRP) ? "r" : "-");
                    strcat(file_permission, (buffer.st_mode & S_IWGRP) ? "w" : "-");
                    strcat(file_permission, (buffer.st_mode & S_IXGRP) ? "x" : "-");
                    strcat(file_permission, (buffer.st_mode & S_IROTH) ? "r" : "-");
                    strcat(file_permission, (buffer.st_mode & S_IWOTH) ? "w" : "-");
                    strcat(file_permission, (buffer.st_mode & S_IXOTH) ? "x" : "-");
                    // Append file_permission the file_permission_array
                    strcpy(file_permission_array[file_length], file_permission);
                }
                file_length++;
            }
        }

        closedir(d);


        // Add element to struct
        FILEINFO file_information[file_length];
        for (int j = 0; j < file_length; ++j) {
            strcpy(file_information[j].file_name, file_array[j]);

            file_information[j].file_size = file_disk_size_array[j];

            strcpy(file_information[j].file_permission, file_permission_array[j]);

            char *date_str = file_time_and_date_array[j];

//                    // Print substring
//                    printf("%.*s\n", 2, date_str + 0);

            // Store substring

            // Substring from index 0 to index 3
            char *year_str = malloc(4);
            strncpy(year_str, date_str + 0, 4);
            int year_integer = atoi(year_str);
            file_information[j].file_year = year_integer;

            // Substring from index 5 to index 6
            char *month_str = malloc(2);
            strncpy(month_str, date_str + 5, 2);
            int month_integer = atoi(month_str);
            file_information[j].file_month = month_integer;

            // Substring from index 8 to index 9
            char *day_str = malloc(2);
            strncpy(day_str, date_str + 8, 2);
            int day_integer = atoi(day_str);
            file_information[j].file_day = day_integer;

            // Substring from index 11 to index 12
            char *hour_str = malloc(2);
            strncpy(hour_str, date_str + 11, 2);
            int hour_integer = atoi(hour_str);
            file_information[j].file_hour = hour_integer;

            // Substring from index 14 to index 15
            char *minute_str = malloc(2);
            strncpy(minute_str, date_str + 14, 2);
            int minute_integer = atoi(minute_str);
            file_information[j].file_minute = minute_integer;

            // Substring from index 17 to index 18
            char *second_str = malloc(2);
            strncpy(second_str, date_str + 17, 2);
            int second_integer = atoi(second_str);
            file_information[j].file_second = second_integer;
        }

        // If the file_array is not empty, it will print the file name
        int file_index = 0;
        int file_n_or_p = 0;
        // If you do not initialize the file_quit, it will go crazy
        int file_quit = 0;
        while (file_index < file_length) {
            if (file_quit == 'q' || file_quit == 'Q') {
                break;
            }

            char print_January_to_December[256] = "";
            for (int j = 0; j < 12; ++j) {
                // date_sorted_file_index is starting at 1
                // j is starting at 0
                // when j = 0
                // j + 1 = 0 + 1 = 1 is the first month
                // month_array[j] is month_array[0], it is the first month, it is January
                if (file_information[file_index].file_month == j + 1) {
                    strcpy(print_January_to_December, month_array[j]);
                }
            }

            printf("           %d\t", file_index);
            printf("File Name: \t\t\t\t\t%s \n "
                   "\t\tFile Size: \t\t\t\t\t%d bytes \n "
                   "\t\tFile Permission: \t\t\t\t%s \n "
                   "\t\tFile Time and Date of Creation: \t\t%d-%s-%d %d:%d:%d \n\n",
                   file_information[file_index].file_name,
                   file_information[file_index].file_size,
                   file_information[file_index].file_permission,
                   file_information[file_index].file_year,
                   print_January_to_December,
                   file_information[file_index].file_day,
                   file_information[file_index].file_hour,
                   file_information[file_index].file_minute,
                   file_information[file_index].file_second);
            file_index++;
            // You can only show 5 lines of file names in one page
            // You will need to implement Prev, and Next Operations so that the menu fits
            // on one screen
            if (file_index % 5 == 0) {
                printf("+-----------------File menu-------------------+ \n");
                printf("|                                             | \n");
                printf("|      Hit N for Next or hit P for prev       | \n");
                printf("|        If you do not want to display        | \n");
                printf("|      additional files, press q to quit      | \n");
                printf("|             displaying files                | \n");
                printf("+---------------------------------------------+ \n");
                printf("Enter your choice: ");
//                printf("Hit N for Next or hit P for prev. "
//                       "If you do not want to display additional files, "
//                       "press q to quit displaying files \n");
                file_n_or_p = getchar();
                // Use getchar() to eat the newline
                getchar();
                file_quit = file_n_or_p;
                if (file_quit == 'q' || file_quit == 'Q') {
                    continue;
                }
                while (true) {
                    if (file_n_or_p == 'n' || file_n_or_p == 'N') {
                        break;
                    }
                    if (file_n_or_p == 'p' || file_n_or_p == 'P') {
                        // User should not press p in the first page
                        // printf("           %d Directories: %s \n", folder_index,
                        // folder_array[folder_index]); is before folder_index++;
                        // Let's say that you are in the page 2,
                        // the file_index is 10, but the last file it will print is at file_index 9
                        // So file_index - 10 is 0, in the next iteration,
                        // it will get started printing at file_index 0, which is the first page
                        // It is the same for page 3, 4, 5, 6...
                        file_index = file_index - 10;

                        // Of course a user may type “P” ten times in succession, so
                        // you shouldn’t try to move the “window” of files
                        // you are showing to “before” the first

                        // If the user press p in the first page
                        // it will tell the user that this is the first page and
                        // it will display files in the first page
                        // so if the file_index is < 0, we need to set it to 0,
                        // in the next iteration, it will get started
                        // printing at file_index 0, which is the first page
                        if (file_index < 0) {
                            printf("+--------------Oops, that's wrong-------------+ \n");
                            printf("|                                             | \n");
                            printf("|     It would not display any page before    | \n");
                            printf("|                the first page               | \n");
                            printf("+---------------------------------------------+ \n");
//                            printf("You reach the first page. There is no file before the file 0. "
//                                   "So it would not display any page before the first page. \n");
                            file_index = 0;
                        }
                        break;
                    }

                    // If the user did not choose N or P,
                    // let the user know that they need to choose N or P
                    printf("+--------------Oops, that's wrong-------------+ \n");
                    printf("|                                             | \n");
                    printf("|        You should choose N or P             | \n");
                    printf("|      Hit N for Next or hit P for prev       | \n");
                    printf("|        If you do not want to display        | \n");
                    printf("|      additional files, press q to quit      | \n");
                    printf("|             displaying files                | \n");
                    printf("+---------------------------------------------+ \n");
                    printf("Enter your choice: ");
//                    printf("You should choose N or P. "
//                           "Hit N for Next or hit P for prev. "
//                           "If you do not want to display additional files, "
//                           "press q to quit displaying files \n");
                    file_n_or_p = getchar();
                    // Use getchar() to eat the newline
                    getchar();
                    file_quit = file_n_or_p;
                    if (file_quit == 'q' || file_quit == 'Q') {
                        break;
                    }
                }
            }
        }

        printf("-----------------------------------------------\n");

        printf("-----------------------------------------------\n");
        printf("                    MAIN MENU \n");
        printf("-----------------------------------------------\n");

        printf("Operation: D  Display \n"
               "           E  Edit \n"
               "           R  Run \n"
               "           C  Change Directory \n"
               "           S  Sort Directory listing \n"
               "           M  Move to Directory \n"
               "           X  Remove File \n"
               "           Q  Quit \n");
        printf("Enter your choice: ");
        // The permission denied happens a lot,
        // so we need to change the default permission
        // Set it to be readable, writable and executable by all users
        char *cmd_chmod = "chmod -R 777 *";
        int main_menu = getchar();
        // Use getchar() to eat the newline
        getchar();

        // If the user press d, it will display directories and files
        if (main_menu == 'd' || main_menu == 'D') {
            continue;
        }

            // If the user press e, it will open a text editor with a file
        else if (main_menu == 'e' || main_menu == 'E') {
            // If there is no file in the current directory,
            // it will not open a text editor
            if (file_length == 0) {
                printf("There is no file in the current directory. "
                       "So it will not edit file. \n");
                continue;
            }

            // If there is file in the current directory,
            // it will let the user choose which file to edit
            char edit[256];
            while (true) {
                printf("Edit what? Choose a file number. \n");
                fgets(edit, 256, stdin);
                // Use strcspn to eat the newline
                edit[strcspn(edit, "\n")] = '\0';

                // Check if the user input is numeric
                // Check if character in edit only contains number from 0 to 9
                bool status = true;
                for (int j = 0; j < strlen(edit); ++j) {
                    if (edit[j] != '0' &&
                        edit[j] != '1' &&
                        edit[j] != '2' &&
                        edit[j] != '3' &&
                        edit[j] != '4' &&
                        edit[j] != '5' &&
                        edit[j] != '6' &&
                        edit[j] != '7' &&
                        edit[j] != '8' &&
                        edit[j] != '9') {
                        status = false;
                    }
                }
                if (status == false) {
                    printf("Your input is not a number or it is a negative number. \n");
                    continue;
                }

                // Check if the input number is starting with 0
                if (edit[0] == '0') {
                    printf("Your input is a number, "
                           "but it is starting with 0. \n");
                    continue;
                }

                // Check if the user input number is greater than the file_length
                if (atoi(edit) >= file_length) {
                    printf("Your input is a number, "
                           "but it is greater than the number of files. \n");
                    continue;
                }
                // If the user input is number and it is less than the total number of the files
                break;
            }

            // Edit the file
            strcpy(cmd, "pico ");
            // Create quotation marks around the name
            strcat(cmd, "\"");
            strcat(cmd, file_array[atoi(edit)]);
            // Create quotation marks around the name
            strcat(cmd, "\"");
            system(cmd_chmod);
            system(cmd);
            continue;
        }

            // If the user press r, it will run an executable program
        else if (main_menu == 'r' || main_menu == 'R') {
            // If there is no file in the current directory,
            // it will let the user choose which file to run
            if (file_length == 0) {
                printf("There is no file in the current directory. "
                       "So it will not run an executable program. \n");
                continue;
            }

            // If there is file in the current directory,
            // it will let the user choose which file to run
            char run[256];
            while (true) {
                printf("Run what? Choose a file number. \n");
                fgets(run, 256, stdin);
                // Use strcspn to eat the newline
                run[strcspn(run, "\n")] = '\0';

                // Check if the user input is numeric
                // Check if character in run only contains number from 0 to 9
                bool status = true;
                for (int j = 0; j < strlen(run); ++j) {
                    if (run[j] != '0' &&
                        run[j] != '1' &&
                        run[j] != '2' &&
                        run[j] != '3' &&
                        run[j] != '4' &&
                        run[j] != '5' &&
                        run[j] != '6' &&
                        run[j] != '7' &&
                        run[j] != '8' &&
                        run[j] != '9') {
                        status = false;
                    }
                }
                if (status == false) {
                    printf("Your input is not a number or it is a negative number. \n");
                    continue;
                }

                // Check if the input number is starting with 0
                if (run[0] == '0') {
                    printf("Your input is a number, "
                           "but it is starting with 0. \n");
                    continue;
                }

                // Check if the user input number is greater than the file_length
                if (atoi(run) >= file_length) {
                    printf("Your input is a number, "
                           "but it is greater than the number of files. \n");
                    continue;
                }
                // If the user input is number and it is less than the total number of the files
                break;
            }

            // Run the file
            strcpy(cmd, "./");
            // Create quotation marks around the name
            strcat(cmd, "\"");
            strcat(cmd, file_array[atoi(run)]);
            // Create quotation marks around the name
            strcat(cmd, "\"");
            system(cmd_chmod);
            system(cmd);
            continue;
        }

            // If the user press c, it will change directory
        else if (main_menu == 'c' || main_menu == 'C') {
            // Check if the folder_array is empty
            if (folder_length == 0) {
                printf("There is no directory in the current directory. "
                       "So it will not change directory. \n");
                continue;
            }

            // If there is directory in the current directory,
            // it will let the user choose which directory to change to
            char change[256];
            while (true) {
                printf("Change to? Choose a directory number. \n");
                fgets(change, 256, stdin);
                // Use strcspn to eat the newline
                change[strcspn(change, "\n")] = '\0';

                // Check if the user input is numeric
                // Check if character in change only contains number from 0 to 9
                bool status = true;
                for (int j = 0; j < strlen(change); ++j) {
                    if (change[j] != '0' &&
                        change[j] != '1' &&
                        change[j] != '2' &&
                        change[j] != '3' &&
                        change[j] != '4' &&
                        change[j] != '5' &&
                        change[j] != '6' &&
                        change[j] != '7' &&
                        change[j] != '8' &&
                        change[j] != '9') {
                        status = false;
                    }
                }
                if (status == false) {
                    printf("Your input is not a number or it is a negative number. \n");
                    continue;
                }

                // Check if the input number is starting with 0
                if (change[0] == '0') {
                    printf("Your input is a number, "
                           "but it is starting with 0. \n");
                    continue;
                }

                // Check if the user input number is greater than the folder_length
                if (atoi(change) >= folder_length) {
                    printf("Your input is a number, "
                           "but it is greater than the number of files. \n");
                    continue;
                }
                // If the user input is number and it is less than the total number of the directories
                break;
            }

            system(cmd_chmod);
            // Change to directory
            strcpy(cmd, folder_array[atoi(change)]);
            if (chdir(cmd) == -1) {
                printf("%s : No such directory.\n", cmd);
            }
            continue;
        }

            // If the user press s, it will sort the file
        else if (main_menu == 's' || main_menu == 'S') {
            char sort[256];
            printf("Press s for sort by size or press d for sort by date. \n");
            fgets(sort, 256, stdin);
            // Use strcspn to eat the newline
            sort[strcspn(sort, "\n")] = '\0';

            if (strcmp(sort, "s") == 0 || strcmp(sort, "S") == 0) {
                // Use bubble sort to sort file size array
                // If you swap size array, you should swap other array,
                // so all info will sort together

                // Let's say that there are n elements in the array
                // We will run the for loop n - 1 times
                // In the n th iteration of the outer for loop, the first element is the smallest element
                for (int starting_index = 0; starting_index < file_length - 1; ++starting_index) {
                    // In the last iteration of the inner for loop,
                    // we will compare the n - 1 th element and the n th element, so it is n - 1.
                    // There is a better way.
                    // The starting_index count how many times the outer for loop run,
                    // let's say that it is i times.
                    // So the last i elements are sorted, so the inner for loop will run n - i times.
                    // Let's say that the last i elements are sorted area and
                    // the other elements are unsorted area.
                    // We will compare the last element in the unsorted area and
                    // the second element in the unsorted area.
                    // We will not compare the last element in the unsorted area and
                    // the first element in the sorted area, so the inner for loop will run n - i - 1 times.
                    for (int file_size_index_right = 0;
                         file_size_index_right < file_length - starting_index - 1; ++file_size_index_right) {
                        if (file_disk_size_array[file_size_index_right] <
                            file_disk_size_array[file_size_index_right + 1]) {

                            // swap file_disk_size_array
                            int file_size_tmp = file_disk_size_array[file_size_index_right];
                            file_disk_size_array[file_size_index_right] = file_disk_size_array[file_size_index_right +
                                                                                               1];
                            file_disk_size_array[file_size_index_right + 1] = file_size_tmp;

                            // swap file_array
                            char file_tmp[256];
                            strcpy(file_tmp, file_array[file_size_index_right]);
                            strcpy(file_array[file_size_index_right], file_array[file_size_index_right + 1]);
                            strcpy(file_array[file_size_index_right + 1], file_tmp);

                            // swap file_time_and_date_array
                            char file_time_tmp[256];
                            strcpy(file_time_tmp, file_time_and_date_array[file_size_index_right]);
                            strcpy(file_time_and_date_array[file_size_index_right],
                                   file_time_and_date_array[file_size_index_right + 1]);
                            strcpy(file_time_and_date_array[file_size_index_right + 1], file_time_tmp);

                            // swap file_permission_array
                            char file_permission_tmp[256];
                            strcpy(file_permission_tmp, file_time_and_date_array[file_size_index_right]);
                            strcpy(file_time_and_date_array[file_size_index_right],
                                   file_time_and_date_array[file_size_index_right + 1]);
                            strcpy(file_time_and_date_array[file_size_index_right + 1], file_permission_tmp);
                        }
                    }
                }

                // Use qsort to sort size
                qsort(file_information, file_length, sizeof(*file_information), cmp_size);

                // Print sorted file in descending order
                printf("=============SORTED FILES BY SIZE==============\n");
                printf("      The file size is in descending order \n");
                int size_sorted_file_index = 0;
                int size_sorted_file_quit = 0;
                while (size_sorted_file_index < file_length) {
                    if (size_sorted_file_quit == 'q' || size_sorted_file_quit == 'Q') {
                        break;
                    }

                    // If you use qsort
                    char print_January_to_December[256] = "";
                    for (int j = 0; j < 12; ++j) {
                        // date_sorted_file_index is starting at 1
                        // j is starting at 0
                        // when j = 0
                        // j + 1 = 0 + 1 = 1 is the first month
                        // month_array[j] is month_array[0], it is the first month, it is January
                        if (file_information[size_sorted_file_index].file_month == j + 1) {
                            strcpy(print_January_to_December, month_array[j]);
                        }
                    }

//                    // If you use bubble sort
//                    printf("           %d\t", size_sorted_file_index);
//                    printf("File Name: \t\t\t\t\t%s \n "
//                           "\t\tFile Size: \t\t\t\t\t%d bytes \n "
//                           "\t\tFile Permission: \t\t\t\t%s \n "
//                           "\t\tFile Time and Date of Creation: \t\t%s \n\n",
//                           file_array[size_sorted_file_index],
//                           file_disk_size_array[size_sorted_file_index],
//                           file_time_and_date_array[size_sorted_file_index],
//                           file_permission_array[size_sorted_file_index]);
                    // If you use qsort
                    printf("           %d\t", size_sorted_file_index);
                    printf("File Name: \t\t\t\t\t%s \n "
                           "\t\tFile Size: \t\t\t\t\t%d bytes \n "
                           "\t\tFile Permission: \t\t\t\t%s \n "
                           "\t\tFile Time and Date of Creation: \t\t%d-%s-%d %d:%d:%d \n\n",
                           file_information[size_sorted_file_index].file_name,
                           file_information[size_sorted_file_index].file_size,
                           file_information[size_sorted_file_index].file_permission,
                           file_information[size_sorted_file_index].file_year,
                           print_January_to_December,
                           file_information[size_sorted_file_index].file_day,
                           file_information[size_sorted_file_index].file_hour,
                           file_information[size_sorted_file_index].file_minute,
                           file_information[size_sorted_file_index].file_second);
                    size_sorted_file_index++;
                    // You can only show 5 lines of file names in one page
                    // You will need to implement Prev, and Next Operations so that the menu fits
                    // on one screen
                    if (size_sorted_file_index % 5 == 0) {
                        printf("+--------------Sorted file menu---------------+ \n");
                        printf("|                                             | \n");
                        printf("|      Hit N for Next or hit P for prev       | \n");
                        printf("|        If you do not want to display        | \n");
                        printf("|      additional files, press q to quit      | \n");
                        printf("|             displaying files                | \n");
                        printf("+---------------------------------------------+ \n");
                        printf("Enter your choice: ");
//                printf("Hit N for Next or hit P for prev. "
//                       "If you do not want to display additional files, "
//                       "press q to quit displaying files \n");
                        file_n_or_p = getchar();
                        // Use getchar() to eat the newline
                        getchar();
                        size_sorted_file_quit = file_n_or_p;
                        if (size_sorted_file_quit == 'q' || size_sorted_file_quit == 'Q') {
                            continue;
                        }
                        while (true) {
                            if (file_n_or_p == 'n' || file_n_or_p == 'N') {
                                break;
                            }
                            if (file_n_or_p == 'p' || file_n_or_p == 'P') {
                                // User should not press p in the first page
                                // printf("           %d Directories: %s \n", folder_index,
                                // folder_array[folder_index]); is before folder_index++;
                                // Let's say that you are in the page 2,
                                // the sorted_file_index is 10, but the last file it will print is at sorted_file_index 9
                                // So sorted_file_index - 10 is 0, in the next iteration,
                                // it will get started printing at sorted_file_index 0, which is the first page
                                // It is the same for page 3, 4, 5, 6...
                                size_sorted_file_index = size_sorted_file_index - 10;

                                // Of course a user may type “P” ten times in succession, so
                                // you shouldn’t try to move the “window” of files
                                // you are showing to “before” the first

                                // If the user press p in the first page
                                // it will tell the user that this is the first page and
                                // it will display files in the first page
                                // so if the sorted_file_index is < 0, we need to set it to 0,
                                // in the next iteration, it will get started
                                // printing at sorted_file_index 0, which is the first page
                                if (size_sorted_file_index < 0) {
                                    printf("+--------------Oops, that's wrong-------------+ \n");
                                    printf("|                                             | \n");
                                    printf("|     It would not display any page before    | \n");
                                    printf("|                the first page               | \n");
                                    printf("+---------------------------------------------+ \n");
//                            printf("You reach the first page. There is no file before the file 0. "
//                                   "So it would not display any page before the first page. \n");
                                    size_sorted_file_index = 0;
                                }
                                break;
                            }

                            // If the user did not choose N or P,
                            // let the user know that they need to choose N or P
                            printf("+--------------Oops, that's wrong-------------+ \n");
                            printf("|                                             | \n");
                            printf("|        You should choose N or P             | \n");
                            printf("|      Hit N for Next or hit P for prev       | \n");
                            printf("|        If you do not want to display        | \n");
                            printf("|      additional files, press q to quit      | \n");
                            printf("|             displaying files                | \n");
                            printf("+---------------------------------------------+ \n");
                            printf("Enter your choice: ");
//                    printf("You should choose N or P. "
//                           "Hit N for Next or hit P for prev. "
//                           "If you do not want to display additional files, "
//                           "press q to quit displaying files \n");
                            file_n_or_p = getchar();
                            // Use getchar() to eat the newline
                            getchar();
                            size_sorted_file_quit = file_n_or_p;
                            if (size_sorted_file_quit == 'q' || size_sorted_file_quit == 'Q') {
                                break;
                            }
                        }
                    }
                }
            }

                // We will use qsort to sort date
            else if (strcmp(sort, "d") == 0 || strcmp(sort, "D") == 0) {

                qsort(file_information, file_length, sizeof(*file_information), cmp_date_time);

                // Print sorted file in ascending order
                printf("=============SORTED FILES BY DATE==============\n");
                printf("      The file date is in chronological order \n");
                int date_sorted_file_index = 0;
                int date_sorted_file_quit = 0;
                while (date_sorted_file_index < file_length) {
                    if (date_sorted_file_quit == 'q' || date_sorted_file_quit == 'Q') {
                        break;
                    }

                    char print_January_to_December[256] = "";
                    for (int j = 0; j < 12; ++j) {
                        // date_sorted_file_index is starting at 1
                        // j is starting at 0
                        // when j = 0
                        // j + 1 = 0 + 1 = 1 is the first month
                        // month_array[j] is month_array[0], it is the first month, it is January
                        if (file_information[date_sorted_file_index].file_month == j + 1) {
                            strcpy(print_January_to_December, month_array[j]);
                        }
                    }

                    printf("           %d\t", date_sorted_file_index);
                    printf("File Name: \t\t\t\t\t%s \n "
                           "\t\tFile Size: \t\t\t\t\t%d bytes \n "
                           "\t\tFile Permission: \t\t\t\t%s \n "
                           "\t\tFile Time and Date of Creation: \t\t%d-%s-%d %d:%d:%d \n\n",
                           file_information[date_sorted_file_index].file_name,
                           file_information[date_sorted_file_index].file_size,
                           file_information[date_sorted_file_index].file_permission,
                           file_information[date_sorted_file_index].file_year,
                           print_January_to_December,
                           file_information[date_sorted_file_index].file_day,
                           file_information[date_sorted_file_index].file_hour,
                           file_information[date_sorted_file_index].file_minute,
                           file_information[date_sorted_file_index].file_second);
                    date_sorted_file_index++;
                    // You can only show 5 lines of file names in one page
                    // You will need to implement Prev, and Next Operations so that the menu fits
                    // on one screen
                    if (date_sorted_file_index % 5 == 0) {
                        printf("+--------------Sorted file menu---------------+ \n");
                        printf("|                                             | \n");
                        printf("|      Hit N for Next or hit P for prev       | \n");
                        printf("|        If you do not want to display        | \n");
                        printf("|      additional files, press q to quit      | \n");
                        printf("|             displaying files                | \n");
                        printf("+---------------------------------------------+ \n");
                        printf("Enter your choice: ");
//                printf("Hit N for Next or hit P for prev. "
//                       "If you do not want to display additional files, "
//                       "press q to quit displaying files \n");
                        file_n_or_p = getchar();
                        // Use getchar() to eat the newline
                        getchar();
                        date_sorted_file_quit = file_n_or_p;
                        if (date_sorted_file_quit == 'q' || date_sorted_file_quit == 'Q') {
                            continue;
                        }
                        while (true) {
                            if (file_n_or_p == 'n' || file_n_or_p == 'N') {
                                break;
                            }
                            if (file_n_or_p == 'p' || file_n_or_p == 'P') {
                                // User should not press p in the first page
                                // printf("           %d Directories: %s \n", folder_index,
                                // folder_array[folder_index]); is before folder_index++;
                                // Let's say that you are in the page 2,
                                // the sorted_file_index is 10, but the last file it will print is at sorted_file_index 9
                                // So sorted_file_index - 10 is 0, in the next iteration,
                                // it will get started printing at sorted_file_index 0, which is the first page
                                // It is the same for page 3, 4, 5, 6...
                                date_sorted_file_index = date_sorted_file_index - 10;

                                // Of course a user may type “P” ten times in succession, so
                                // you shouldn’t try to move the “window” of files
                                // you are showing to “before” the first

                                // If the user press p in the first page
                                // it will tell the user that this is the first page and
                                // it will display files in the first page
                                // so if the sorted_file_index is < 0, we need to set it to 0,
                                // in the next iteration, it will get started
                                // printing at sorted_file_index 0, which is the first page
                                if (date_sorted_file_index < 0) {
                                    printf("+--------------Oops, that's wrong-------------+ \n");
                                    printf("|                                             | \n");
                                    printf("|     It would not display any page before    | \n");
                                    printf("|                the first page               | \n");
                                    printf("+---------------------------------------------+ \n");
//                            printf("You reach the first page. There is no file before the file 0. "
//                                   "So it would not display any page before the first page. \n");
                                    date_sorted_file_index = 0;
                                }
                                break;
                            }

                            // If the user did not choose N or P,
                            // let the user know that they need to choose N or P
                            printf("+--------------Oops, that's wrong-------------+ \n");
                            printf("|                                             | \n");
                            printf("|        You should choose N or P             | \n");
                            printf("|      Hit N for Next or hit P for prev       | \n");
                            printf("|        If you do not want to display        | \n");
                            printf("|      additional files, press q to quit      | \n");
                            printf("|             displaying files                | \n");
                            printf("+---------------------------------------------+ \n");
                            printf("Enter your choice: ");
//                    printf("You should choose N or P. "
//                           "Hit N for Next or hit P for prev. "
//                           "If you do not want to display additional files, "
//                           "press q to quit displaying files \n");
                            file_n_or_p = getchar();
                            // Use getchar() to eat the newline
                            getchar();
                            date_sorted_file_quit = file_n_or_p;
                            if (date_sorted_file_quit == 'q' || date_sorted_file_quit == 'Q') {
                                break;
                            }
                        }
                    }
                }
            }

            continue;

        }

            // If the user press m, it will move file to directory
        else if (main_menu == 'm' || main_menu == 'M') {
            // If there is no file in the current directory,
            // it will not move file to directory
            if (file_length == 0) {
                printf("There is no file in the current directory. "
                       "So it will not move file to directory. \n");
                continue;
            }

            // If there is file in the current directory,
            // it will let the user choose move what file to which directory
            char move_what[256];
            while (true) {
                printf("Move what? Choose a file number. \n");
                fgets(move_what, 256, stdin);
                // Use strcspn to eat the newline
                move_what[strcspn(move_what, "\n")] = '\0';

                // Check if the user input is numeric
                // Check if character in move_what only contains number from 0 to 9
                bool status = true;
                for (int j = 0; j < strlen(move_what); ++j) {
                    if (move_what[j] != '0' &&
                        move_what[j] != '1' &&
                        move_what[j] != '2' &&
                        move_what[j] != '3' &&
                        move_what[j] != '4' &&
                        move_what[j] != '5' &&
                        move_what[j] != '6' &&
                        move_what[j] != '7' &&
                        move_what[j] != '8' &&
                        move_what[j] != '9') {
                        status = false;
                    }
                }
                if (status == false) {
                    printf("Your input is not a number or it is a negative number. \n");
                    continue;
                }

                // Check if the input number is starting with 0
                if (move_what[0] == '0') {
                    printf("Your input is a number, "
                           "but it is starting with 0. \n");
                    continue;
                }

                // Check if the user input number is greater than the file_length
                if (atoi(move_what) >= file_length) {
                    printf("Your input is a number, "
                           "but it is greater than the number of files. \n");
                    continue;
                }
                // If the user input is number and it is less than the total number of the files
                break;
            }

            // Move the file
            strcpy(cmd, "mv ");
            // Create quotation marks around the name
            strcat(cmd, "\"");
            strcat(cmd, file_array[atoi(move_what)]);
            // Create quotation marks around the name
            strcat(cmd, "\"");

            // Check if the folder_array is empty
            if (folder_length == 0) {
                printf("There is no directory in the current directory. "
                       "So it will not change directory. \n");
                continue;
            }

            // If there is directory in the current directory,
            // it will let the user choose which directory to change to
            char to_which_directory[256];
            while (true) {
                printf("Move to which directory? Choose a directory number. \n");
                fgets(to_which_directory, 256, stdin);
                // Use strcspn to eat the newline
                to_which_directory[strcspn(to_which_directory, "\n")] = '\0';

                // Check if the user input is numeric
                // Check if character in to_which_directory only contains number from 0 to 9
                bool status = true;
                for (int j = 0; j < strlen(to_which_directory); ++j) {
                    if (to_which_directory[j] != '0' &&
                        to_which_directory[j] != '1' &&
                        to_which_directory[j] != '2' &&
                        to_which_directory[j] != '3' &&
                        to_which_directory[j] != '4' &&
                        to_which_directory[j] != '5' &&
                        to_which_directory[j] != '6' &&
                        to_which_directory[j] != '7' &&
                        to_which_directory[j] != '8' &&
                        to_which_directory[j] != '9') {
                        status = false;
                    }
                }
                if (status == false) {
                    printf("Your input is not a number or it is a negative number. \n");
                    continue;
                }

                // Check if the input number is starting with 0
                if (to_which_directory[0] == '0') {
                    printf("Your input is a number, "
                           "but it is starting with 0. \n");
                    continue;
                }

                // Check if the user input number is greater than the folder_length
                if (atoi(to_which_directory) >= folder_length) {
                    printf("Your input is a number, "
                           "but it is greater than the number of files. \n");
                    continue;
                }
                // If the user input is number and it is less than the total number of the directories
                break;
            }

            // Move file to which directory
            strcat(cmd, " ");
            // Create quotation marks around the name
            strcat(cmd, "\"");
            strcat(cmd, folder_array[atoi(to_which_directory)]);
            // Create quotation marks around the name
            strcat(cmd, "\"");
            system(cmd_chmod);
            system(cmd);
            continue;
        }

            // If the user press x, it will remove the file
        else if (main_menu == 'x' || main_menu == 'X') {
            // If there is no file in the current directory,
            // it will not remove the file
            if (file_length == 0) {
                printf("There is no file in the current directory. "
                       "So it will not remove the file. \n");
                continue;
            }

            // If there is file in the current directory,
            // it will let the user choose which file to remove
            char remove[256];
            while (true) {
                printf("Remove what? Choose a file number. \n");
                fgets(remove, 256, stdin);
                // Use strcspn to eat the newline
                remove[strcspn(remove, "\n")] = '\0';

                // Check if the user input is numeric
                // Check if character in remove only contains number from 0 to 9
                bool status = true;
                for (int j = 0; j < strlen(remove); ++j) {
                    if (remove[j] != '0' &&
                        remove[j] != '1' &&
                        remove[j] != '2' &&
                        remove[j] != '3' &&
                        remove[j] != '4' &&
                        remove[j] != '5' &&
                        remove[j] != '6' &&
                        remove[j] != '7' &&
                        remove[j] != '8' &&
                        remove[j] != '9') {
                        status = false;
                    }
                }
                if (status == false) {
                    printf("Your input is not a number or it is a negative number. \n");
                    continue;
                }

                // Check if the input number is starting with 0
                if (remove[0] == '0') {
                    printf("Your input is a number, "
                           "but it is starting with 0. \n");
                    continue;
                }

                // Check if the user input number is greater than the file_length
                if (atoi(remove) >= file_length) {
                    printf("Your input is a number, "
                           "but it is greater than the number of files. \n");
                    continue;
                }
                // If the user input is number and it is less than the total number of the files
                break;
            }

            // Remove the file
            strcpy(cmd, "rm ");
            // Create quotation marks around the name
            strcat(cmd, "\"");
            strcat(cmd, file_array[atoi(remove)]);
            // Create quotation marks around the name
            strcat(cmd, "\"");
            system(cmd_chmod);
            system(cmd);
            continue;
        }

            // If the user press q, it will quit
        else if (main_menu == 'q' || main_menu == 'Q') {
            printf("+---------------------------Thank you----------------------------+\n");
            printf("|                                                                |\n");
            printf("|          Thank you for your interest in the myshell program    |\n");
            printf("|                                                                |\n");
            printf("+----------------------------------------------------------------+\n");
            exit(0);
        }

            // If the user press other keys
        else {
            printf("We do not have the operation. \n");
            continue;
        }
    }

    return 0;
}

/*This code was taken from the website
 * Source code website:
 * https://www.daniweb.com/programming/software-development/threads/405653/qsort-an-array-of-structures-by-2-criteria
 * Source code:
 *  #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    struct record {
        char name[32];
        double value;
    };
    int compare(const void *a, const void *b)
    {
        const struct record *pa = a;
        const struct record *pb = b;

        int diff = strcmp(pa->name, pb->name);

        if (diff == 0) {
            if (pa->value < pb->value)
                diff = -1;
            else if (pa->value > pb->value)
                diff = +1;
            else
                diff = 0;
        }

        return diff;
    }
    int main(void)
    {
        struct record records[] = {
            {"A", 1},
            {"B", 13},
            {"B", 1},
            {"B", 2},
            {"C", 3},
            {"C", 1}
        };
        int size = 6;
        int i;

        qsort(records, size, sizeof(*records), compare);

        for (i = 0; i < 6; i++)
            printf("%s %f\n", records[i].name, records[i].value);

        return 0;
    }
 *
 * What I have learned:
 * I learned how to use qsort to sort dates chronologically
 * */

// cmp_year_month sorts year and month
int cmp_year_month(const void *a, const void *b) {
    const struct fileInfo *ptr_a = a;
    const struct fileInfo *ptr_b = b;

    int diff_year = ptr_a->file_year - ptr_b->file_year;

    if (diff_year == 0) {
        if (ptr_a->file_month < ptr_b->file_month)
            diff_year = -1;
        else if (ptr_a->file_month > ptr_b->file_month)
            diff_year = +1;
        else
            diff_year = 0;
    }

    return diff_year;

}

// cmp_date_time sorts year, month, day, hour, minute and second
int cmp_date_time(const void *a, const void *b) {
    const FILEINFO *ptr_a = a;
    const FILEINFO *ptr_b = b;

    int diff_year = ptr_a->file_year - ptr_b->file_year;

    // If the years are the same
    if (diff_year == 0) {
        // compare month
        if (ptr_a->file_month < ptr_b->file_month)
            diff_year = -1;
        else if (ptr_a->file_month > ptr_b->file_month)
            diff_year = 1;
            // If the months are the same
        else {
            // compare days
            if (ptr_a->file_day < ptr_b->file_day)
                diff_year = -1;
            else if (ptr_a->file_day > ptr_b->file_day)
                diff_year = 1;
                // If the days are the same
            else {
                // compare hours
                if (ptr_a->file_hour < ptr_b->file_hour)
                    diff_year = -1;
                else if (ptr_a->file_hour > ptr_b->file_hour)
                    diff_year = 1;
                    // If the hours are the same
                else {
                    // compare minutes
                    if (ptr_a->file_minute < ptr_b->file_minute)
                        diff_year = -1;
                    else if (ptr_a->file_minute > ptr_b->file_minute)
                        diff_year = 1;
                        // If the minutes are the same
                    else {
                        // compare seconds
                        if (ptr_a->file_second < ptr_b->file_second)
                            diff_year = -1;
                        else if (ptr_a->file_second > ptr_b->file_second)
                            diff_year = 1;
                        else
                            diff_year = 0;
                    }
                }
            }
        }
    }

    return diff_year;

}

// cmp_size sorts size
int cmp_size(const void *a, const void *b) {
    const FILEINFO *ptr_a = a;
    const FILEINFO *ptr_b = b;

    return ptr_b->file_size - ptr_a->file_size;

}
