#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct record {
    char name[32];
    double value;
};

int compare(const void *a, const void *b) {
    const struct record *pa = a;
    const struct record *pb = b;

    int diff = strcmp(pa->name, pb->name);

    if (diff == 0) {
        if (pa->value < pb->value)
            diff = -1;
        else if (pa->value > pb->value)
            diff = +1;
        else
            diff = 0;
    }

    return diff;
}

int main(void) {
    struct record records[] = {
            {"A", 1},
            {"B", 13},
            {"B", 1},
            {"B", 2},
            {"C", 3},
            {"C", 1}
    };
    int size = 6;
    int i;

    qsort(records, size, sizeof(*records), compare);

    for (i = 0; i < 6; i++)
        printf("%s %f\n", records[i].name, records[i].value);

    return 0;
}
