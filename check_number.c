#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>


bool check_number(char *number_str);

int main() {
    char str[256];
    while (true) {
        printf("Enter a number \n");
        fgets(str, 256, stdin);
        str[strcspn(str, "\n")] = '\0';

        bool status = true;
        for (int i = 0; i < strlen(str); ++i) {
            if (isdigit(str[i]) == 0) {
                printf("It is not digit \n");
                status = false;
                break;
            }
        }

        if (status == false) {
            continue;
        } else {
            break;
        }
    }

    printf("It is digit \n");

    return 0;

}
