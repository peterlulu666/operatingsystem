#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <dirent.h>
#include <sys/stat.h>

// The DABPT should be allocated from disk blocks,
// 4 entries per block, where each entry
// should contain a file meta-information (FileSize, last time+date (secs), pointers to
// data blocks), user name
typedef struct DABPTMetaInfo {
    // FileSize
    int fileSize;
    // last time+date (secs)
    char last_time_date[256];
    // pointers to data blocks
    int pointerToDataBlocks;
    char userName[256];
} DABPTMetaInfo;

// The FNT should be of size allocated, each entry should
// contain a 50 char (maximum) file name and an inode pointer (index to DABPT)(blocks)
typedef struct fnt {
    // contain a 50 char
    char fileName[50];
    // inode pointer
    DABPTMetaInfo inodePointer;
} FNT;

// The Block Pointer Table has direct pointers to data blocks, and one additional
// pointer to another entry in the Table, if needed (for big files), these may be
// chained for very large files. (Similar to CP/M extents)
typedef struct blockPointerTable {
    // direct pointers to data blocks
    int directPointers;
    // pointer to another entry in the Table
    struct blockPointerTable *anotherPointer;
} BlockPointerTable;

// Your FS should have 4 (or more, if easier to implement) sections:
// A FileNameTable (FNT), a directory and a disk attribute/block pointer table (DABPT),
// and the data blocks
typedef struct fileSystem {
    // file system name
    char fileSystemName[256];
    // number of blocks
    int numberOfBlocks;
    // time and date
    char dateTime[256];
    // FileNameTable
    int numberOfFNT;
    int *indexOfFNT;
    // disk attribute/block pointer table
    int numberOfDABPTEntries;
    int *indexOfDABPTEntries;
    // block pointer table
    int numberOfBlockPointerTable;
    int *indexOfBPT;
    // data blocks
    char **dataBlocks;
    int *indexOfDataBlocks;
    FNT *fnt;
    DABPTMetaInfo *dabpt;
    BlockPointerTable *bpt;
} FileSystem;


int main() {
    char *choose = malloc(256);
    printf("Choose 1 for Createfs \n "
           "Choose 2 for Formatfs \n "
           "Choose 3 for Savefs \n "
           "Choose 4 for Openfs \n "
           "Choose 5 for List \n");
    fgets(choose, 256, stdin);
    // Use strcspn to eat the newline
    choose[strcspn(choose, "\n")] = '\0';

    FileSystem fileSystem;
    if (strcmp(choose, "1") == 0) {
        // Createfs #ofblocks – creates a filesystem (disk) with #ofblocks size, each 256 bytes
        // For example Createfs 250 creates a “virtual disk”, a file that will be initialized to
        // 250 blocks of 256 bytes each. It is created in memory, and initialized
        char blocks[256];
        printf("Enter number of blocks \n");
        fgets(blocks, 256, stdin);

        // Use strcspn to eat the newline
        blocks[strcspn(blocks, "\n")] = '\0';

        fileSystem.numberOfBlocks = atoi((blocks));

        // every block has 256 bytes
        int blockSize = 256;
        char **virtualDisk;
        virtualDisk = (char **) malloc(blockSize * sizeof(char *));
        for (int i = 0; i < fileSystem.numberOfBlocks; i++) {
            virtualDisk[i] = (char *) malloc(fileSystem.numberOfBlocks * sizeof(char));
        }

        fileSystem.indexOfDataBlocks = malloc(fileSystem.numberOfBlocks * sizeof(int *));
        fileSystem.dataBlocks = virtualDisk;

        printf("File System has been created with %d data blocks. \n\n", fileSystem.numberOfBlocks);
    } else if (strcmp(choose, "2") == 0) {
        // Formatfs #filenames #DABPTentries
        // For example Formatfs 64 48 reserves space for 64 file names and 48 file meta data,
        // Note that some file names may “point” to the same file metadata, in this example
        // there can only be 48 unique files
        printf("Enter the file system names \n");
        fgets(fileSystem.fileSystemName, 256, stdin);

        // Use strcspn to eat the newline
        fileSystem.fileSystemName[strcspn(fileSystem.fileSystemName, "\n")] = '\0';

        char fileNameNumber[256];
        printf("Enter number of file names \n");
        fgets(fileNameNumber, 256, stdin);

        // Use strcspn to eat the newline
        fileNameNumber[strcspn(fileNameNumber, "\n")] = '\0';

        char DABPTentriesNumber[256];
        printf("Enter number of DABPTentries \n");
        fgets(DABPTentriesNumber, 256, stdin);

        // Use strcspn to eat the newline
        DABPTentriesNumber[strcspn(DABPTentriesNumber, "\n")] = '\0';

        fileSystem.numberOfFNT = atoi(fileNameNumber);
        fileSystem.numberOfDABPTEntries = atoi(DABPTentriesNumber);
        fileSystem.dabpt = malloc(fileSystem.numberOfDABPTEntries * sizeof(DABPTMetaInfo));
        fileSystem.bpt = malloc(fileSystem.numberOfDABPTEntries * sizeof(BlockPointerTable));
        fileSystem.indexOfFNT = malloc(fileSystem.numberOfFNT * sizeof(int *));
        fileSystem.indexOfDABPTEntries = malloc(fileSystem.numberOfDABPTEntries * sizeof(int *));
        fileSystem.indexOfBPT = malloc(fileSystem.numberOfDABPTEntries * sizeof(int *));

        printf("File System has been formatted. There are %d files and %d DABPT entries. \n\n", fileSystem.numberOfFNT,
               fileSystem.numberOfDABPTEntries);
    } else if (strcmp(choose, "3") == 0) {
        // Savefs name– save the “disk” image in a file “name”
        char diskImageName[256];
        printf("Enter the disk image name \n");
        fgets(diskImageName, 256, stdin);

        // Use strcspn to eat the newline
        diskImageName[strcspn(diskImageName, "\n")] = '\0';

        // time and date
        time_t t;
        struct tm *tmp;
        char dateTime[50];
        time(&t);
        tmp = localtime(&t);
        strftime(dateTime, sizeof(dateTime), "%x,%I:%M%p", tmp);
        strcpy(fileSystem.dateTime, dateTime);
        // store the data to the file
        FILE *saveToFile = fopen(diskImageName, "w");
        fprintf(saveToFile, "%d,%d,%d,%d,%s,%s\n", fileSystem.numberOfBlocks, fileSystem.numberOfDABPTEntries,
                fileSystem.numberOfFNT, fileSystem.numberOfBlockPointerTable, fileSystem.fileSystemName,
                fileSystem.dateTime);
        fseek(saveToFile, 0, SEEK_END);
        fclose(saveToFile);
    } else if (strcmp(choose, "4") == 0) {
        // Openfs name- use an existing disk image
        // the openfs command retrieves the image from the file and puts into memory
        char openName[256];
        printf("Open what \n");
        fgets(openName, 256, stdin);

        // Use strcspn to eat the newline
        openName[strcspn(openName, "\n")] = '\0';

        FILE *openFileSystem = fopen(openName, "r");
        printf("%s has been opened \n", openName);
    } else if (strcmp(choose, "5") == 0) {
        // List – list files in a FS
        // List what is in “your” directory

        // we will use stat system call to list the file
        printf("File in the directory: \n");
        struct dirent *de;
        struct stat buffer;
        DIR *d;
        d = opendir(".");
        char filePath[256] = "./";
        while ((de = readdir(d))) {
            if (!stat(filePath, &buffer)) {
                if (S_ISDIR(buffer.st_mode)) {
                    printf("%s \n", de->d_name);
                }
            }
        }
        closedir(d);

        // we will list the meta-information
        printf("FIle System Information: \n");
        printf("File system name: %s \n", fileSystem.fileSystemName);
        printf("The number of blocks: %d \n", fileSystem.numberOfBlocks);
        printf("The date and time: %s \n", fileSystem.dateTime);
        printf("The number of FNT is: %d \n", fileSystem.numberOfFNT);
        printf("The number of BPT is: %d \n", fileSystem.numberOfBlockPointerTable);
        printf("The number of DABPT is : %d \n", fileSystem.numberOfDABPTEntries);
    }

    return 0;

}















