#include <stdlib.h>
#include <stdio.h>
#include <zconf.h>

char **str_to_arr();

int main() {
    __auto_type token = str_to_arr();
    printf("%s\n", token[0]);
    execvp(token[0], token);
    return 0;
}

char **str_to_arr() {
    char **token = malloc(100 * sizeof(char **));
    token[0] = "ls";
    token[1] = "-a";
    token[2] = NULL;

    return token;

}
