/*
 * run
 * gcc wang_7416.c -Wno-int-to-void-pointer-cast
 * ./a.out
 * */
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <zconf.h>
#include <pthread.h>

// the earthquake data is stored in struct
typedef struct earthquake {
    char time[256];
    double latitude;
    double longitude;
    double depth;
    double mag;
    char magType[256];
    int nst;
    double gap;
    double dmin;
    double rms;
    char net[256];
    char id[256];
    char updated[256];
    char place[256];
    char type[256];
    double horizontalError;
    double depthError;
    double magError;
    int magNst;
    char status[256];
    char locationSource[256];
    char magSource[256];
} Earthquake;

Earthquake readData(char fileName[256], Earthquake *earthquakeData);

int countData(char fileName[256]);

void storeData(char fileName[256], Earthquake *earthquakeData, int startLine, int finishLine);

int cmp_latitude(const void *a, const void *b);

void mergeFile(int numberOfFile);

void *runOneThread(void *threadid);

void *runTwoThread(void *threadid);

void *runFourThread(void *threadid);

void *runTenThread(void *threadid);

int main() {
    int rc;
    int i;

    char *choose = malloc(256);
    printf("Choose 1 for 1 concurrent threads, 2 for 2 concurrent threads, 4 for 4 concurrent threads, and 10 for 10 concurrent threads \n");
    fgets(choose, 256, stdin);

    // Use strcspn to eat the newline
    choose[strcspn(choose, "\n")] = '\0';

    // at this point, we will start bubble sort
    clock_t start = clock();

    if (strcmp(choose, "1") == 0) {
        int NUM_THREADS = 1;
        pthread_t threads[NUM_THREADS];
        for (i = 0; i < NUM_THREADS; i++) {
            /* How to make compiler not show int to void pointer cast warnings
             * https://stackoverflow.com/questions/22751762/how-to-make-compiler-not-show-int-to-void-pointer-cast-warnings
             */
            rc = pthread_create(&threads[i], NULL, runOneThread, (void *) i);
            if (rc) {
                printf("Error:unable to create thread, %d\n", rc);
                exit(-1);
            }
        }

        // wait for thread termination
        for (int j = 0; j < NUM_THREADS; i++) {
            pthread_join(threads[j], NULL);
            j++;
        }

        // at this point, we will finish bubble sort
        clock_t finish = clock();

        // in order to get the number of seconds, we will need to divide by CLOCKS_PER_SEC
        double run_time = (double) (finish - start) / CLOCKS_PER_SEC;

        // print the run time
        printf("The run time is %f seconds. \n", run_time);

        pthread_exit(NULL);

    } else if (strcmp(choose, "2") == 0) {
        int NUM_THREADS = 2;
        pthread_t threads[NUM_THREADS];
        for (i = 0; i < NUM_THREADS; i++) {
            rc = pthread_create(&threads[i], NULL, runTwoThread, (void *) i);
            if (rc) {
                printf("Error:unable to create thread, %d\n", rc);
                exit(-1);
            }
        }

        // wait for thread termination
        for (int j = 0; j < NUM_THREADS; i++) {
            pthread_join(threads[j], NULL);
            j++;
        }

        mergeFile(2);

        // at this point, we will finish bubble sort
        clock_t finish = clock();

        // in order to get the number of seconds, we will need to divide by CLOCKS_PER_SEC
        double run_time = (double) (finish - start) / CLOCKS_PER_SEC;

        // print the run time
        printf("The run time is %f seconds. \n", run_time);

        pthread_exit(NULL);

    } else if (strcmp(choose, "4") == 0) {
        int NUM_THREADS = 4;
        pthread_t threads[NUM_THREADS];
        for (i = 0; i < NUM_THREADS; i++) {
            rc = pthread_create(&threads[i], NULL, runFourThread, (void *) i);
            if (rc) {
                printf("Error:unable to create thread, %d\n", rc);
                exit(-1);
            }
        }

        // wait for thread termination
        for (int j = 0; j < NUM_THREADS; i++) {
            pthread_join(threads[j], NULL);
            j++;
        }

        mergeFile(4);

        // at this point, we will finish bubble sort
        clock_t finish = clock();

        // in order to get the number of seconds, we will need to divide by CLOCKS_PER_SEC
        double run_time = (double) (finish - start) / CLOCKS_PER_SEC;

        // print the run time
        printf("The run time is %f seconds. \n", run_time);

        pthread_exit(NULL);

    } else if (strcmp(choose, "10") == 0) {
        int NUM_THREADS = 10;
        pthread_t threads[NUM_THREADS];
        for (i = 0; i < NUM_THREADS; i++) {
            rc = pthread_create(&threads[i], NULL, runTenThread, (void *) i);
            if (rc) {
                printf("Error:unable to create thread, %d\n", rc);
                exit(-1);
            }
        }

        // wait for thread termination
        for (int j = 0; j < NUM_THREADS; i++) {
            pthread_join(threads[j], NULL);
            j++;
        }

        mergeFile(10);

        // at this point, we will finish bubble sort
        clock_t finish = clock();

        // in order to get the number of seconds, we will need to divide by CLOCKS_PER_SEC
        double run_time = (double) (finish - start) / CLOCKS_PER_SEC;

        // print the run time
        printf("The run time is %f seconds. \n", run_time);

        pthread_exit(NULL);

    } else {
        printf("You should choose 1, 2, 4, and 10 \n");
        exit(0);
    }

    return 0;

}

void *runOneThread(void *threadid) {
    /*Multithreading in C
 * https://www.tutorialspoint.com/multithreading-in-c
 * */
    long tid;
    tid = (long) threadid;

    Earthquake *earthquakeData = (Earthquake *) malloc(INT16_MAX * sizeof(Earthquake));
    *earthquakeData = readData("all_month.csv", earthquakeData);
    int numberOfLine = countData("all_month.csv");

    if (tid == 0) {
        // bubble sort
        for (int j = 0; j < numberOfLine; ++j) {
            for (int i = 0; i < numberOfLine; ++i) {
                if (earthquakeData[i].latitude > earthquakeData[i + 1].latitude) {
                    Earthquake tmp;
                    tmp = earthquakeData[i];
                    earthquakeData[i] = earthquakeData[i + 1];
                    earthquakeData[i + 1] = tmp;
                }
            }
        }

        // store the data to the file
        storeData("mergedData1Thread.csv", earthquakeData, 0, numberOfLine);
    }

    pthread_exit(NULL);

}

void *runTwoThread(void *threadid) {
    /*Multithreading in C
     * https://www.tutorialspoint.com/multithreading-in-c
     * */
    long tid;
    tid = (long) threadid;

    Earthquake *earthquakeData = (Earthquake *) malloc(INT16_MAX * sizeof(Earthquake));
    *earthquakeData = readData("all_month.csv", earthquakeData);
    int numberOfLine = countData("all_month.csv");

    if (tid == 0) {
        // bubble sort
        for (int j = 0 * (numberOfLine / 2); j < 1 * (numberOfLine / 2); ++j) {
            for (int i = 0 * (numberOfLine / 2); i < 1 * (numberOfLine / 2); ++i) {
                if (earthquakeData[i].latitude > earthquakeData[i + 1].latitude) {
                    Earthquake tmp;
                    tmp = earthquakeData[i];
                    earthquakeData[i] = earthquakeData[i + 1];
                    earthquakeData[i + 1] = tmp;
                }
            }
        }

        // store the data to the file
        storeData("sorted1.csv", earthquakeData, 0 * (numberOfLine / 2), 1 * (numberOfLine / 2));
    } else if (tid == 1) {
        // bubble sort
        for (int j = 1 * (numberOfLine / 2); j < 2 * (numberOfLine / 2); ++j) {
            for (int i = 1 * (numberOfLine / 2); i < 2 * (numberOfLine / 2); ++i) {
                if (earthquakeData[i].latitude > earthquakeData[i + 1].latitude) {
                    Earthquake tmp;
                    tmp = earthquakeData[i];
                    earthquakeData[i] = earthquakeData[i + 1];
                    earthquakeData[i + 1] = tmp;
                }
            }
        }

        // store the data to the file
        storeData("sorted2.csv", earthquakeData, 1 * (numberOfLine / 2), 2 * (numberOfLine / 2));
    }

    pthread_exit(NULL);

}

void *runFourThread(void *threadid) {
    /*Multithreading in C
     * https://www.tutorialspoint.com/multithreading-in-c
     * */
    long tid;
    tid = (long) threadid;

    Earthquake *earthquakeData = (Earthquake *) malloc(INT16_MAX * sizeof(Earthquake));
    *earthquakeData = readData("all_month.csv", earthquakeData);
    int numberOfLine = countData("all_month.csv");

    if (tid == 0) {
        // bubble sort
        for (int j = 0 * (numberOfLine / 4); j < 1 * (numberOfLine / 4); ++j) {
            for (int i = 0 * (numberOfLine / 4); i < 1 * (numberOfLine / 4); ++i) {
                if (earthquakeData[i].latitude > earthquakeData[i + 1].latitude) {
                    Earthquake tmp;
                    tmp = earthquakeData[i];
                    earthquakeData[i] = earthquakeData[i + 1];
                    earthquakeData[i + 1] = tmp;
                }
            }
        }

        // store the data to the file
        storeData("sorted1.csv", earthquakeData, 0 * (numberOfLine / 4), 1 * (numberOfLine / 4));
    } else if (tid == 1) {
        // bubble sort
        for (int j = 1 * (numberOfLine / 4); j < 2 * (numberOfLine / 4); ++j) {
            for (int i = 1 * (numberOfLine / 4); i < 2 * (numberOfLine / 4); ++i) {
                if (earthquakeData[i].latitude > earthquakeData[i + 1].latitude) {
                    Earthquake tmp;
                    tmp = earthquakeData[i];
                    earthquakeData[i] = earthquakeData[i + 1];
                    earthquakeData[i + 1] = tmp;
                }
            }
        }

        // store the data to the file
        storeData("sorted2.csv", earthquakeData, 1 * (numberOfLine / 4), 2 * (numberOfLine / 4));
    } else if (tid == 2) {
        // bubble sort
        for (int j = 2 * (numberOfLine / 4); j < 3 * (numberOfLine / 4); ++j) {
            for (int i = 2 * (numberOfLine / 4); i < 3 * (numberOfLine / 4); ++i) {
                if (earthquakeData[i].latitude > earthquakeData[i + 1].latitude) {
                    Earthquake tmp;
                    tmp = earthquakeData[i];
                    earthquakeData[i] = earthquakeData[i + 1];
                    earthquakeData[i + 1] = tmp;
                }
            }
        }

        // store the data to the file
        storeData("sorted3.csv", earthquakeData, 2 * (numberOfLine / 4), 3 * (numberOfLine / 4));
    } else if (tid == 3) {
        // bubble sort
        for (int j = 3 * (numberOfLine / 4); j < 4 * (numberOfLine / 4); ++j) {
            for (int i = 3 * (numberOfLine / 4); i < 4 * (numberOfLine / 4); ++i) {
                if (earthquakeData[i].latitude > earthquakeData[i + 1].latitude) {
                    Earthquake tmp;
                    tmp = earthquakeData[i];
                    earthquakeData[i] = earthquakeData[i + 1];
                    earthquakeData[i + 1] = tmp;
                }
            }
        }

        // store the data to the file
        storeData("sorted4.csv", earthquakeData, 3 * (numberOfLine / 4), 4 * (numberOfLine / 4));
    }

    pthread_exit(NULL);

}

void *runTenThread(void *threadid) {
    /*Multithreading in C
     * https://www.tutorialspoint.com/multithreading-in-c
     * */
    long tid;
    tid = (long) threadid;

    Earthquake *earthquakeData = (Earthquake *) malloc(INT16_MAX * sizeof(Earthquake));
    *earthquakeData = readData("all_month.csv", earthquakeData);
    int numberOfLine = countData("all_month.csv");

    if (tid == 0) {
        // bubble sort
        for (int j = 0 * (numberOfLine / 10); j < 1 * (numberOfLine / 10); ++j) {
            for (int i = 0 * (numberOfLine / 10); i < 1 * (numberOfLine / 10); ++i) {
                if (earthquakeData[i].latitude > earthquakeData[i + 1].latitude) {
                    Earthquake tmp;
                    tmp = earthquakeData[i];
                    earthquakeData[i] = earthquakeData[i + 1];
                    earthquakeData[i + 1] = tmp;
                }
            }
        }

        storeData("sorted1.csv", earthquakeData, 0 * (numberOfLine / 10), 1 * (numberOfLine / 10));
    } else if (tid == 1) {
        // bubble sort
        for (int j = 1 * (numberOfLine / 10); j < 2 * (numberOfLine / 10); ++j) {
            for (int i = 1 * (numberOfLine / 10); i < 2 * (numberOfLine / 10); ++i) {
                if (earthquakeData[i].latitude > earthquakeData[i + 1].latitude) {
                    Earthquake tmp;
                    tmp = earthquakeData[i];
                    earthquakeData[i] = earthquakeData[i + 1];
                    earthquakeData[i + 1] = tmp;
                }
            }
        }

        storeData("sorted2.csv", earthquakeData, 1 * (numberOfLine / 10), 2 * (numberOfLine / 10));
    } else if (tid == 2) {
        // bubble sort
        for (int j = 2 * (numberOfLine / 10); j < 3 * (numberOfLine / 10); ++j) {
            for (int i = 2 * (numberOfLine / 10); i < 3 * (numberOfLine / 10); ++i) {
                if (earthquakeData[i].latitude > earthquakeData[i + 1].latitude) {
                    Earthquake tmp;
                    tmp = earthquakeData[i];
                    earthquakeData[i] = earthquakeData[i + 1];
                    earthquakeData[i + 1] = tmp;
                }
            }
        }

        storeData("sorted3.csv", earthquakeData, 2 * (numberOfLine / 10), 3 * (numberOfLine / 10));
    } else if (tid == 3) {
        // bubble sort
        for (int j = 3 * (numberOfLine / 10); j < 4 * (numberOfLine / 10); ++j) {
            for (int i = 3 * (numberOfLine / 10); i < 4 * (numberOfLine / 10); ++i) {
                if (earthquakeData[i].latitude > earthquakeData[i + 1].latitude) {
                    Earthquake tmp;
                    tmp = earthquakeData[i];
                    earthquakeData[i] = earthquakeData[i + 1];
                    earthquakeData[i + 1] = tmp;
                }
            }
        }

        storeData("sorted4.csv", earthquakeData, 3 * (numberOfLine / 10), 4 * (numberOfLine / 10));
    } else if (tid == 4) {
        // bubble sort
        for (int j = 4 * (numberOfLine / 10); j < 5 * (numberOfLine / 10); ++j) {
            for (int i = 4 * (numberOfLine / 10); i < 5 * (numberOfLine / 10); ++i) {
                if (earthquakeData[i].latitude > earthquakeData[i + 1].latitude) {
                    Earthquake tmp;
                    tmp = earthquakeData[i];
                    earthquakeData[i] = earthquakeData[i + 1];
                    earthquakeData[i + 1] = tmp;
                }
            }
        }

        storeData("sorted5.csv", earthquakeData, 4 * (numberOfLine / 10), 5 * (numberOfLine / 10));
    } else if (tid == 5) {
        // bubble sort
        for (int j = 5 * (numberOfLine / 10); j < 6 * (numberOfLine / 10); ++j) {
            for (int i = 5 * (numberOfLine / 10); i < 6 * (numberOfLine / 10); ++i) {
                if (earthquakeData[i].latitude > earthquakeData[i + 1].latitude) {
                    Earthquake tmp;
                    tmp = earthquakeData[i];
                    earthquakeData[i] = earthquakeData[i + 1];
                    earthquakeData[i + 1] = tmp;
                }
            }
        }

        storeData("sorted6.csv", earthquakeData, 5 * (numberOfLine / 10), 6 * (numberOfLine / 10));
    } else if (tid == 6) {
        // bubble sort
        for (int j = 6 * (numberOfLine / 10); j < 7 * (numberOfLine / 10); ++j) {
            for (int i = 6 * (numberOfLine / 10); i < 7 * (numberOfLine / 10); ++i) {
                if (earthquakeData[i].latitude > earthquakeData[i + 1].latitude) {
                    Earthquake tmp;
                    tmp = earthquakeData[i];
                    earthquakeData[i] = earthquakeData[i + 1];
                    earthquakeData[i + 1] = tmp;
                }
            }
        }

        storeData("sorted7.csv", earthquakeData, 6 * (numberOfLine / 10), 7 * (numberOfLine / 10));
    } else if (tid == 7) {
        // bubble sort
        for (int j = 7 * (numberOfLine / 10); j < 8 * (numberOfLine / 10); ++j) {
            for (int i = 7 * (numberOfLine / 10); i < 8 * (numberOfLine / 10); ++i) {
                if (earthquakeData[i].latitude > earthquakeData[i + 1].latitude) {
                    Earthquake tmp;
                    tmp = earthquakeData[i];
                    earthquakeData[i] = earthquakeData[i + 1];
                    earthquakeData[i + 1] = tmp;
                }
            }
        }

        storeData("sorted8.csv", earthquakeData, 7 * (numberOfLine / 10), 8 * (numberOfLine / 10));
    } else if (tid == 8) {
        // bubble sort
        for (int j = 8 * (numberOfLine / 10); j < 9 * (numberOfLine / 10); ++j) {
            for (int i = 8 * (numberOfLine / 10); i < 9 * (numberOfLine / 10); ++i) {
                if (earthquakeData[i].latitude > earthquakeData[i + 1].latitude) {
                    Earthquake tmp;
                    tmp = earthquakeData[i];
                    earthquakeData[i] = earthquakeData[i + 1];
                    earthquakeData[i + 1] = tmp;
                }
            }
        }

        storeData("sorted9.csv", earthquakeData, 8 * (numberOfLine / 10), 9 * (numberOfLine / 10));
    } else if (tid == 9) {
        // bubble sort
        for (int j = 9 * (numberOfLine / 10); j < 10 * (numberOfLine / 10); ++j) {
            for (int i = 9 * (numberOfLine / 10); i < 10 * (numberOfLine / 10); ++i) {
                if (earthquakeData[i].latitude > earthquakeData[i + 1].latitude) {
                    Earthquake tmp;
                    tmp = earthquakeData[i];
                    earthquakeData[i] = earthquakeData[i + 1];
                    earthquakeData[i + 1] = tmp;
                }
            }
        }

        storeData("sorted10.csv", earthquakeData, 9 * (numberOfLine / 10), 10 * (numberOfLine / 10));
    }

    pthread_exit(NULL);

}

Earthquake readData(char fileName[256], Earthquake *earthquakeData) {
    // read the data
    FILE *file = fopen(fileName, "r");
    char line[1024];
    char delimiter[256] = ",";
    int numberOfLine = 0;
    /*This code was taken from the website
     * Source code website:
     * https://stackoverflow.com/questions/44217917/read-specific-line-from-csv-file-in-c
     * Source code:
     *  char buff[1024];
     *  fgets(buff, 1024, fp);
     *  fgets(buff, 1024, fp);
     *  What I have learned:
     *  I learned how to read data from the second line
     * */
    // read the header
    fgets(line, 1024, file);
    // read the data from the second line
    while (fgets(line, 1024, file) != NULL) {
        char *tmp = tmp = strdup(line);

        // store time data to the struct
        char *timeStr = strsep(&tmp, delimiter);
        strcpy(earthquakeData[numberOfLine].time, timeStr);

        // store latitude data to the struct
        char *latitudeStr = strsep(&tmp, delimiter);
        // atof() will convert string to double
        // there are some blank entries,
        // store the blank entries as empty string and atof() will automatically convert it to 0
        double latitude = atof(latitudeStr);
        earthquakeData[numberOfLine].latitude = latitude;

        // store longitude data to the struct
        char *longitudeStr = strsep(&tmp, delimiter);
        // atof() will convert string to double
        // there are some blank entries,
        // store the blank entries as empty string and atof() will automatically convert it to 0
        double longitude = atof(longitudeStr);
        earthquakeData[numberOfLine].longitude = longitude;

        // store depth data to the struct
        char *depthStr = strsep(&tmp, delimiter);
        // atof() will convert string to double
        // there are some blank entries,
        // store the blank entries as empty string and atof() will automatically convert it to 0
        double depth = atof(depthStr);
        earthquakeData[numberOfLine].depth = depth;

        // store mag data to the struct
        char *magStr = strsep(&tmp, delimiter);
        // atof() will convert string to double
        // there are some blank entries,
        // store the blank entries as empty string and atof() will automatically convert it to 0
        double mag = atof(magStr);
        earthquakeData[numberOfLine].mag = mag;

        // store magType data to the struct
        char *magTypeStr = strsep(&tmp, delimiter);
        strcpy(earthquakeData[numberOfLine].magType, magTypeStr);

        // store nst data to the struct
        char *nstStr = strsep(&tmp, delimiter);
        // atof() will convert string to double
        // there are some blank entries,
        // store the blank entries as empty string and atof() will automatically convert it to 0
        double nst = atof(nstStr);
        earthquakeData[numberOfLine].nst = nst;

        // store gap data to the struct
        char *gapStr = strsep(&tmp, delimiter);
        // atof() will convert string to double
        // there are some blank entries,
        // store the blank entries as empty string and atof() will automatically convert it to 0
        double gap = atof(gapStr);
        earthquakeData[numberOfLine].gap = gap;

        // store dmin data to the struct
        char *dminStr = strsep(&tmp, delimiter);
        // atof() will convert string to double
        // there are some blank entries,
        // store the blank entries as empty string and atof() will automatically convert it to 0
        double dmin = atof(dminStr);
        earthquakeData[numberOfLine].dmin = dmin;

        // store rms data to the struct
        char *rmsStr = strsep(&tmp, delimiter);
        // atof() will convert string to double
        // there are some blank entries,
        // store the blank entries as empty string and atof() will automatically convert it to 0
        double rms = atof(rmsStr);
        earthquakeData[numberOfLine].rms = rms;

        // store net data to the struct
        char *netStr = strsep(&tmp, delimiter);
        strcpy(earthquakeData[numberOfLine].net, netStr);

        // store id data to the struct
        char *idStr = strsep(&tmp, delimiter);
        strcpy(earthquakeData[numberOfLine].id, idStr);

        // store updated data to the struct
        char *updatedStr = strsep(&tmp, delimiter);
        strcpy(earthquakeData[numberOfLine].updated, updatedStr);

        // store place data to the struct
        // the place contains quotation mark and comma
        strsep(&tmp, "\"");
        char *placeStr = strsep(&tmp, "\"");
        strcpy(earthquakeData[numberOfLine].place, placeStr);
        strsep(&tmp, delimiter);

        // store type data to the struct
        char *typeStr = strsep(&tmp, delimiter);
        strcpy(earthquakeData[numberOfLine].type, typeStr);

        // store horizontalError data to the struct
        earthquakeData[numberOfLine].horizontalError = atof(strsep(&tmp, delimiter));
        // store depthError data to the struct
        earthquakeData[numberOfLine].depthError = atof(strsep(&tmp, delimiter));
        // store magError data to the struct
        earthquakeData[numberOfLine].magError = atof(strsep(&tmp, delimiter));
        // store magNst data to the struct
        earthquakeData[numberOfLine].magNst = atof(strsep(&tmp, delimiter));
        // store status data to the struct
        strcpy(earthquakeData[numberOfLine].status, strsep(&tmp, delimiter));
        // store locationSource data to the struct
        strcpy(earthquakeData[numberOfLine].locationSource, strsep(&tmp, delimiter));
        // store magSource data to the struct
        strcpy(earthquakeData[numberOfLine].magSource, strsep(&tmp, delimiter));

        numberOfLine++;
        free(tmp);
    }

    // close stream
    fclose(file);

    return *earthquakeData;

}

int countData(char fileName[256]) {
    // read the header
    FILE *file = fopen(fileName, "r");
    char line[1024];
    int numberOfLine = 0;

    fgets(line, 1024, file);
    // read the data from the second line
    while (fgets(line, 1024, file) != NULL) {
        numberOfLine++;
    }

    // close stream
    fclose(file);

    return numberOfLine;

}

void storeData(char fileName[256], Earthquake *earthquakeData, int startLine, int finishLine) {
    // store the data to the file
    FILE *saveToFile = fopen(fileName, "w");
    fprintf(saveToFile,
            "time,latitude,longitude,depth,mag,magType,nst,gap,dmin,rms,net,id,updated,place,type,horizontalError,"
            "depthError,magError,magNst,status,locationSource,magSource\n");
    for (int k = startLine; k < finishLine; ++k) {
        fprintf(saveToFile, "%s,%f,%f,%f,%f,%s,%d,%f,%f,%f,%s,%s,%s,\"%s\",%s,%f,%f,%f,%d,%s,%s,%s",
                earthquakeData[k].time,
                earthquakeData[k].latitude,
                earthquakeData[k].longitude,
                earthquakeData[k].depth,
                earthquakeData[k].mag,
                earthquakeData[k].magType,
                earthquakeData[k].nst,
                earthquakeData[k].gap,
                earthquakeData[k].dmin,
                earthquakeData[k].rms,
                earthquakeData[k].net,
                earthquakeData[k].id,
                earthquakeData[k].updated,
                earthquakeData[k].place,
                earthquakeData[k].type,
                earthquakeData[k].horizontalError,
                earthquakeData[k].depthError,
                earthquakeData[k].magError,
                earthquakeData[k].magNst,
                earthquakeData[k].status,
                earthquakeData[k].locationSource,
                earthquakeData[k].magSource);
    }

    // close stream
    fclose(saveToFile);

}

// cmp_latitude sorts latitude
int cmp_latitude(const void *a, const void *b) {
    const Earthquake *ptr_a = a;
    const Earthquake *ptr_b = b;

    return ptr_b->latitude - ptr_a->latitude;

}

void mergeFile(int numberOfFile) {
    Earthquake *mergedDataAll = (Earthquake *) malloc(INT16_MAX * sizeof(Earthquake));
    Earthquake *tmpPartData = (Earthquake *) malloc(INT16_MAX * sizeof(Earthquake));
    int countTmpPartData = 0;
    int countMergedDataAll = 0;

    for (int n = 0; n < numberOfFile; ++n) {
        char fileName[256];
        char str[256];

        // create the file name
        strcpy(fileName, "sorted");
        sprintf(str, "%d", n + 1);
        strcat(fileName, str);
        strcat(fileName, ".csv");

        countTmpPartData = countData(fileName);
        int sum = 0;

        for (int k = 0; k < n; ++k) {
            sum = sum + countTmpPartData;
        }

        *tmpPartData = readData(fileName, tmpPartData);

        countMergedDataAll = countMergedDataAll + countTmpPartData;
        for (int l = 1; l < countTmpPartData; ++l) {
            mergedDataAll[l - 1 + sum] = tmpPartData[l];
        }
    }

    qsort(mergedDataAll, countMergedDataAll, sizeof(*mergedDataAll), cmp_latitude);

    char fileName[256];
    char str[256];

    // create the file name
    strcpy(fileName, "mergedData");
    sprintf(str, "%d", numberOfFile);
    strcat(fileName, str);
    strcat(fileName, "Thread.csv");

    storeData(fileName, mergedDataAll, 0, countMergedDataAll);

}


