#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <zconf.h>
#include <ctype.h>


#define MAX_COMMAND_SIZE 255

#define MAX_NUM_ARGUMENTS 5

#define WHITESPACE " \t\n"

#define NUMBER_OF_HISTORY 15

#define NUMBER_OF_PID 15

#define MAX_HISTORY_ARRAY_SIZE 100

#define MAX_PID_ARRAY_SIZE 100

// Create the history_array to store the history of commands
char history_array[MAX_HISTORY_ARRAY_SIZE][MAX_COMMAND_SIZE];
// Count the number of elements in the history_array
int history_array_length = 0;

// Create the pid_array to store the pid
int pid_array[MAX_PID_ARRAY_SIZE];
// Count the number of elements in the pid_array
int pid_array_length = 0;


char **str_to_arr(char *cmd_str);

void nth_command_in_history(char *cmd_str);

void command_is_history();

void run_user_command(char **token);

void command_is_showpids();


int main() {
    // Create the cmd_str to store user input
    char *cmd_str = (char *) malloc(MAX_COMMAND_SIZE);
    while (true) {
        // Print prompt
        printf("msh> ");
        // Read the user input
        fgets(cmd_str, MAX_COMMAND_SIZE, stdin);
        printf("The user command is %s\n", cmd_str);

        // Convert string to array
        char **token = str_to_arr(cmd_str);

        // If the user types a blank line,
        // your shell will, quietly and with no other output,
        // print another prompt and accept a new line of input
        if (token[0] == NULL) {
            continue;
        }

        // Check if the user input is quit or exit
        if (strcmp(cmd_str, "quit") == 0 || strcmp(cmd_str, "exit") == 0) {
            // The shell will exit with status zero if user input is quit or exit
            exit(0);
        }

        // If user input is starting with !
        if (*token[0] == '!') {
            // we will check if n is an integer number >= 1
            // we will re-run the nth command and add the nth command to the history_array
            nth_command_in_history(cmd_str);
            continue;
        }

        // If user input is not starting with !, we will not re-run the nth command
        if (*token[0] != '!') {
            // we will add cmd_str to the history_array
            strcpy(history_array[history_array_length], cmd_str);
            history_array_length = history_array_length + 1;
        }

        // we will run whatever the user command is

        // Check if the user input is history
        if (strcmp(token[0], "history") == 0) {
            // Print the last 15 history
            command_is_history();
            continue;
        }

        // Check if the user input is showpids
        if (strcmp(token[0], "showpids") == 0) {
            command_is_showpids();
            continue;
        }

        // run user command
        run_user_command(token);

    }

    return 0;
}

char **str_to_arr(char *cmd_str) {
    /* Parse input */
    // It will only take 5 arguments
    // The user input cannot have more than 5 arguments
    // Let's say the user input is ls -a -b -t -m -n,
    // so -n will not go into token,
    // so ls will not have option -n,
    // so it will not display user and group IDs numerically.
    char **token = malloc(MAX_NUM_ARGUMENTS * sizeof(char **));

    int token_count = 0;

    // Pointer to point to the token
    // parsed by strsep
    char *arg_ptr;

    char *working_str = strdup(cmd_str);


    // we are going to move the working_str pointer so
    // keep track of its original value so we can deallocate
    // the correct amount at the end
    char *working_root = working_str;

    // Tokenize the input strings with whitespace used as the delimiter
    while (((arg_ptr = strsep(&working_str, WHITESPACE)) != NULL) &&
           (token_count < MAX_NUM_ARGUMENTS)) {
        token[token_count] = strndup(arg_ptr, MAX_COMMAND_SIZE);
        if (strlen(token[token_count]) == 0) {
            token[token_count] = NULL;
        }
        token_count++;
    }


    free(working_root);
    return token;

}

void nth_command_in_history(char *cmd_str) {
    // Check if history_array is empty
    if (history_array_length <= 0) {
        printf("Command not in history. \n");
        return;

    }

    // we will store the number to user_input_n_str
    // Substring starting from index 1
    char *user_input_n_str = &cmd_str[1];

    // Check if the user input is numeric
    // Check if the first character is 0
    if (user_input_n_str[0] == '0') {
        printf("Command not in history. \n");
        return;
    }
    // Check if character in user_input_n_str only contains number from 0 to 9
    for (int i = 0; i < strlen(user_input_n_str) - 1; ++i) {
        if (user_input_n_str[i] != '0' &&
            user_input_n_str[i] != '1' &&
            user_input_n_str[i] != '2' &&
            user_input_n_str[i] != '3' &&
            user_input_n_str[i] != '4' &&
            user_input_n_str[i] != '5' &&
            user_input_n_str[i] != '6' &&
            user_input_n_str[i] != '7' &&
            user_input_n_str[i] != '8' &&
            user_input_n_str[i] != '9') {
            printf("Command not in history. \n");
            return;
        }
    }

    printf("good \n");

}

void command_is_history() {
    int count_history = 0;
    // If there is less than 15 elements in the array start printing from the index 0
    // If there is more than 15 elements in the array start printing from the starting index
    int starting_index = history_array_length - NUMBER_OF_HISTORY;
    for (int i = history_array_length <= NUMBER_OF_HISTORY ? 0 : starting_index;
         i < history_array_length; ++i) {
        count_history = count_history + 1;
        // The output shall be a list of
        // numbers 1 through n and their commands, each on a separate line, single spaced
        printf("%d: %s", count_history, history_array[i]);
    }
}

void command_is_showpids() {
    int count_pid = 0;
    // If there is less than 15 pid in the list,
    // print pid from index 0 to length of pid_list.
    // If there is more than 15 pid in the list,
    // print pid from starting_index to length of pid_list.
    int starting_index = pid_array_length - NUMBER_OF_PID;
    for (int i = pid_array_length <= MAX_PID_ARRAY_SIZE ? 0 : starting_index; i < pid_array_length; ++i) {
        printf("%d: %d \n", count_pid + 1, pid_array[i]);
        count_pid = count_pid + 1;

    }

}


/* name: run_user_command
 * parameter: char **token
 * Does: take the user input and run the command*/
void run_user_command(char **token) {
    // Create child process
    // We will call execvp() in the child process
    pid_t pid = fork();
    // If the pid is -1 the fork failed
    if (pid == -1) {
        printf("fork failed. \n");
        exit(0);

    }

    // If the pid is 0 it will be the child process
    if (pid == 0) {
        int ret = execvp(token[0], &token[0]);
        // Check if the command is supported
        if (ret == -1) {
//            perror("Command not found.\n");
            printf("%s: Command not found.\n", token[0]);
            exit(0);

        }

    }

        // If the pid is > 0 it will be the parent process, we will add the pid to pid_array
    else if (pid > 0) {
        pid_array[pid_array_length] = pid;
        pid_array_length = pid_array_length + 1;

    }

    // The parent process will wait for the child process to be terminated
    wait(NULL);

}

