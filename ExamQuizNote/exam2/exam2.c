#include <stdlib.h>
#include <stdio.h>
#include <zconf.h>
#include <pthread.h>
#include <memory.h>

#define NUM_THREADS 2

int numberArray[INT16_MAX];
int numberOfLine = 0;
int largestFirst = 0;
int largestSecond = 0;
int smallestFirst = 0;
int smallestSecond = 0;
int largestNumber = 0;
int smallestNumber = 0;

void *findRange(void *threadid) {
    long tid;
    tid = (long) threadid;
    if (tid == 0) {
        // bubble sort
        for (int j = 0 * (numberOfLine / 2); j < 1 * (numberOfLine / 2); ++j) {
            for (int i = 0 * (numberOfLine / 2); i < 1 * (numberOfLine / 2); ++i) {
                if (numberArray[i] > numberArray[i + 1]) {
                    int tmp;
                    tmp = numberArray[i];
                    numberArray[i] = numberArray[i + 1];
                    numberArray[i + 1] = tmp;
                }
            }
        }
        // the largest number in the first half numberArray
        largestFirst = numberArray[0];
        // the smallest number in the first half numberArray
        smallestFirst = numberArray[1 * (numberOfLine / 2) - 1];
    } else {
        // bubble sort
        for (int j = 1 * (numberOfLine / 2); j < 2 * (numberOfLine / 2); ++j) {
            for (int i = 1 * (numberOfLine / 2); i < 2 * (numberOfLine / 2); ++i) {
                if (numberArray[i] > numberArray[i + 1]) {
                    int tmp;
                    tmp = numberArray[i];
                    numberArray[i] = numberArray[i + 1];
                    numberArray[i + 1] = tmp;
                }
            }
        }
        // the largest number in the second half numberArray
        largestSecond = numberArray[1 * (numberOfLine / 2)];
        // the smallest number in the second half numberArray
        smallestSecond = numberArray[2 * (numberOfLine / 2) - 1];
    }
    pthread_exit(NULL);
}

int main() {
    // read the data
    FILE *file = fopen("f.txt", "r");
    char line[1024];
    while (fgets(line, 1024, file) != NULL) {
        char *tmp = tmp = strdup(line);

        // store number to the numberArray
        char *numberStr = strsep(&tmp, "");
        numberArray[numberOfLine] = atof(numberStr);

        // increment numberOfLine
        numberOfLine++;
        // free tmp
        free(tmp);

    }

    pthread_t threads[NUM_THREADS];
    int rc;
    int i;
    for (i = 0; i < NUM_THREADS; i++) {
        rc = pthread_create(&threads[i], NULL, findRange, (void *) i);
        if (rc) {
            printf("Error:unable to create thread, %d\n", rc);
            exit(-1);
        }
    }

    // find the largest number in the file
    if (largestFirst >= largestSecond) {
        largestNumber = largestFirst;
    } else {
        largestNumber = largestSecond;
    }

    // find the smallest number in the file
    if (smallestFirst <= smallestSecond) {
        smallestNumber = smallestFirst;
    } else {
        smallestNumber = smallestSecond;
    }

    printf("The range in the file is from %d to %d \n", largestNumber, smallestNumber);

    pthread_exit(NULL);

    return 0;

}



