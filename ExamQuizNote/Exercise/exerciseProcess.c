#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <zconf.h>

int main() {
    // read the data
    FILE *file = fopen("numbers.txt", "r");
    char line[1024];
    int numberOfLine = 0;
    int numberArray[INT16_MAX];
    while (fgets(line, 1024, file) != NULL) {
        char *tmp = tmp = strdup(line);

        // store number to the numberArray
        char *numberStr = strsep(&tmp, "");
        numberArray[numberOfLine] = atof(numberStr);

        // increment numberOfLine
        numberOfLine++;
        // free tmp
        free(tmp);

    }

    pid_t pid = fork();
    // If the pid is -1 the fork failed
    if (pid == -1) {
        printf("fork failed. \n");
        exit(0);

    }

    int largestFirst = 0;
    int largestSecond = 0;

    // If the pid is 0 it will be the child process
    if (pid == 0) {
        // bubble sort
        for (int j = 0 * (numberOfLine / 2); j < 1 * (numberOfLine / 2); ++j) {
            for (int i = 0 * (numberOfLine / 2); i < 1 * (numberOfLine / 2); ++i) {
                if (numberArray[i] > numberArray[i + 1]) {
                    int tmp;
                    tmp = numberArray[i];
                    numberArray[i] = numberArray[i + 1];
                    numberArray[i + 1] = tmp;
                }
            }
        }
        // the largest number in the first half numberArray
        largestFirst = numberArray[0];
    }
        // If the pid is > 0 it will be the parent process
    else if (pid > 0) {
        // bubble sort
        for (int j = 0 * (numberOfLine / 2); j < 1 * (numberOfLine / 2); ++j) {
            for (int i = 0 * (numberOfLine / 2); i < 1 * (numberOfLine / 2); ++i) {
                if (numberArray[i] > numberArray[i + 1]) {
                    int tmp;
                    tmp = numberArray[i];
                    numberArray[i] = numberArray[i + 1];
                    numberArray[i + 1] = tmp;
                }
            }
        }
        // the largest number in the second half numberArray
        largestSecond = numberArray[numberOfLine / 2];
    }

    int largestNumber = 0;

    // find the largest number in the file
    if (largestFirst >= largestSecond) {
        largestNumber = largestFirst;
    } else {
        largestNumber = largestSecond;
    }

    printf("The largest number in the file is %d \n", largestNumber);

    return 0;

}

