## Chapter 1
1. what is OS
    - It provides services to users and programmers that make it possible to utilize a computer without having to deal with the low-level, difficult-to-use hardware commands
    
2. why learn OS
    - OS exists everywhere
3. why contains OS
    - consolidate the common functions needed by the various games installed on the game system
4. user view and system view
    - user view, it is about how user and program utilize the OS
system view, it is about how the OS software actually does the required action
5. user interested in what aspects of OS
    - TABLE 1.1
6. open source benefit
7. device, controller, and driver
    - device, a physical hardware
    - controller, electronic interface
    - driver, a software routine that is part of the OS
8. abstraction
    - allow the main or higher-level part of the OS to deal with all devices of the same type
    the OS provided the same interface to the hardware for all the different devices supported by that OS
    - the standardized interface that provides general system functions but hides the messy hardware details
9. abstraction reason
    - People want to write more interesting and useful programs. In order for programmers to be able to create programs that would run on these different systems with minor or no changes required to the program when moving it to a different system, the OS provided the same interface to the hardware for all the different devices supported by that OS
10. OS and kernel
    - kernel is the core program of the OS. It provides basic functionalities and it is present in memory
    OS contains kernel and more than that

## Chapter 2
1. resources that OS manage
    - the processor (CPU), main memory, I/O devices, and files
    
2. difference between a program and a process
    - a program is a file stored in storage
    - A process is a running or executing program. To be a process, a program needs to have been started by the OS
    program is the code stored in disk      
3. states
    - new, ready, wait, run, and exit
4. how many processes at the same time
    - one or more
5. what causes a transition from the run state to the terminate state
    - a process reaching its end or having a fatal error
6. process is waiting
    - the process requires some resource that is not available or if it needs some I/O to occur
    waiting for a keystroke or reading from a file
7. PCB that is not current
    - the values stored in the program counter register and other processor registers
8. user, system, and OS processes example
    - user, a process that is running an accounting program or a database transaction or a computer game
    system, programming language compilers and program development environments
    OS, memory management, process scheduling, device control, interrupt handling, file services, and network services
9. why need OS to manage hardware
    - OS helps to communicate the hardware components
10. why divide OS into modules
    - new device drivers could be added to the kernel without drastically affecting the other OS modules, which provide memory management
11. what is the microkernel
    - microkernel includes only basic functionality, usually the interfaces to the various types of device driver. It only includes the code run in supervisor mode
    - the code is minimized and there is only essential code in kernel to run privileged instructions  
12. why would use less efficient object-oriented programming
    - the key feature of an object is that the internal structure of an object is hidden and any access to the data contained in an object is through the methods of the object. This makes it less likely that an application can misuse an object and cause problems for other modules
13. mechanism to handle the interrupt
    - The OS will perform the requested service and then return control to the application
14. ask the OS for something
    - using the system call
15. virtual machine architectures
    - application virtual machines
    - Common Language Runtime
16. standard OS API
    - NEXTSTEP OS
    - BeOS
    - a kernel module is written in C or assembler
17. OS with more functions or less functions
    - Less functions. The functions occupied memory. Memory is limited and expensive. More functions are not only occupied memory, but make the OS less efficient. More functions have more bugs, and are difficult to maintain

## Chpater 3
1. PC monitor programs functions        
    - loading and starting a particular application program
        
1. monitor program would allow an application to do simple, common tasks
    - output a character to a device such as a video display or Teletype
    - get a character from the keyboard device
    - save the contents of all or part of memory to a cassette tape or floppy disk
    - restore memory from a saved image on tape or disk
    - print a character to the printer 
1. monitor program error checking
    - After the monitor outputs the character, it returns a status code in register A that indicates OK or not OK. Not OK indicated some exceptional condition
    - result is that Not OK indicated some exceptional condition such as a device that is not responding or illegal values for some of the parameters to the function. The application should look at the status code in register A to determine an appropriate action in case of error
1. early monitors characteristic in the environment that led to the development of real OS CP/M
    - memory requirements are small
    - provide a few functions and the functions required time and expertise to develop, debug, and build
    - there is no standard interface
    - it is not portable 
1. CP/M characteristic and constraints
    - limited memory
    - reliable, not easy to update
    - important functions
    - allow new functionality in later version
2. CP/M supported only one program at a time
    - BIOS, provide standard functions and interfaces, CP/M has BIOS, Palm does not, BIOS contains configuration info, CP/M needs to fit into different screen, Palm is fixed
    - BDOS, kernel, call primitive services in the BIOS
    - CCP is called shell, the CCP is a program
    - TPA is the Transient Program Area
2. overriding overlays design decisions
    - memory is small. So if the application is not able to fit into the remaining available memory, the overlays technique will be used to replace the old section with the new one
    - only overlay code, not stack and data 
    - large program is divided into main part and other sections. The main part is in memory. Others would replace one another in memory as overlays
    - an initialization phase, a main computation loop, and some final reporting phase, these parts of a program do not need to be in memory at the same time, so these parts of the program  overlay  one another in memory
2. Disk and Interrupt handling in early OSs
    - The disk block size and format was fixed for both floppy and hard disks in early PCs
    - there was no need for switching between applications. So The main types of interrupts were for handling I/O devices 
2. input output I/O problem in early OSs
    - the lack of flexibility in handling keyboard input and screen output
    - performance 
2. important things for early OSs
    - keyboard
    - video 
    - disk system
    - file system
2. user input command is the program on the disk. If the command is mapped to the program, you can run it. If not, where is it
    - it will check if it is the name of the shell built-in command
    - let's say that command ls is the binary file on the disk
    - let's say that history command is the shell built in, it is part of bash itself
2. Disk track and sector 
    - there are 77 track
    - track contains 26 sector
    - sector contains 128 bytes of data
    - standard disk, 77 (tracks)  *  26 (track contains 26 sector)  *  128 (sector contains 128 bytes of data)  *  2 (sides) = 512,512 bytes
4. Memory transient program area from low to high
    - program code
    - program static data 
    - heap, it is used for malloc and new 
    - stack, it is used for variable
5. How many files stored in disk
    - small, depends on directory entries
    - large, depends on disk size
5. the OS located in the highest part of memory
    - In the CP/M environment, the OS resided at highest locations in memory. Application programs will start at location 100 to avoid the interrupt vector in low memory, and grew upward
    - the operating system would be generated (configured) to occupy the highest locations in memory, leaving a fixed address—always 100 Hex—to load programs
    - If the OS reside in the high memory, it will not force any program to change address
    - if the OS is upgraded it will not necessary to relink all the application programs
5. multitasking in early OSs
    - do work in parallel, the background printing and foreground editing, the ability to print a file in a background process while editing in a foreground process
    - A small program was loaded into memory at the highest location of memory, immediately below the OS
6. three part floppy disk
    - track, head and hole
6. floppy disk head rotates a certain portion of a surface
    - it is track
    - divide track into smaller pieces, which is sector
    - sector is the smallest addressable portion of the disk
    - sector is called block       

## Chapter 4
1. why Palm multiprocessing OS
    - support a few processes at the same time
    - it display one program on the screen, but it will run multiple OS processes and applications at the same time
    
2. Palm unusual design
    - multiprogramming
    - scheduling of application processes        
    - give top priority to servicing the interface with the user
    - no secondary storage. It uses RAM to store data
    - Small screen size
    - Keyboard is not standard
    - CPU is slow to reduce battery drain
3. Palm is microkernel
4. Why is memory allocated to a process accessed indirectly through the MPT?
    - it is returned a master chunk pointer, which is the offset in the MPT of the pointer to that chunk
5. free chunks memory
    - “first fit” and “best fit”
6. databases
    - The chunks are aggregated into collections
7. Palm not use compression
    - the CPU power in the Palm OS platforms is also modest     
8. Palm abstraction
    - Some programs want to see every keystroke
    - it is available for applications that only want to read an entire line of input
9. event driven
    - If there are no events for it to process then it tells the OS that it wants to WAIT for more events. When another event occurs the OS will pass the information to the application as a return from one of the calls to check for various classes of events
    - The OS therefore has to maintain a queue of the events that have happened but that have not been given to the application yet. This queue is maintained in priority order so that more important events can be processed first by the application
10. Palm scheduler task
    - preemptive multitasking schedule
    - it is prepared to run many tasks and shifting tasks 
    - scheduler will run high priority tasks and interrupt low priority tasks

## Chapter 5
1. first GUI interface
    - Xerox ALTO
    
2. first commercially successful inexpensive cheap affordable personal computers with GUI
    - Macintosh personal computer
3. Macintosh CPU
    - 68000 CPU 
    - address memory segments of 64 KB at one time              
4. Mac OS single tasking
    - the Mac OS did not initially allow more than one program to run at the same time, even for background printing
    - why they forego Apple Lisa multitasking 
        - The memory is expensive. In order to provide the affordable product, Macintosh had to run with very limited memory
5. Mac OS files and folders
    - keeps all files in a single directory
    - folders, grouping files is useful
    - supports single directory
6. Mac OS provide no memory protection
7. Mac OS difficulty problem stack heap
    - Since many applications call multiple levels of subroutines, sometimes recursively, this stack tends to grow as the program runs
    - A big problem for the designers of the Macintosh was how to make optimum use of the 128 KB of RAM
    - how to avoid the problem
        - the system heap used by the OS, and the application heap
    - problem caused by heap memory 
        - crashes caused by application program errors manipulating the system heap,
    - how to deal with it
        - use ROM to boot 
8. Mac OS in ROM
    - avoid filling the limited storage available on a floppy disk
    - helped the system to boot faster
9. Mac OS cut paste
    - cut paste takes in seconds instead of minutes
        - more RAM        
        - Switcher program
        - multitasking
        - The user could then switch between these applications by clicking an icon on the menu bar. The current application would horizontally slide out of view, and the next one would slide in
10. Switcher program
    - it switched the OS from running one application to running another
    - allocated a separate heap for each application
    - it makes the Mac OS a multitasking, but it is very limited multitasking
11. Hierarchical File System
    - it is the subdirectories, but not tags, folders contain folders
    - directory entries contain file metadata something like file name, type and timestamp 
12. Mac OS MultiFinder
    - it allowed each program to keep running, giving each application CPU time
    - MultiFinder allowed applications to use the layering model to coexist
    - MultiFinder provided a way to allocate RAM to each program depending on the need
13. Mac OS cooperative multitasking
    - a process can run as long as it wants to
14. System 6 32-bit addressing
    - compatibility problem. The ROM code was not 32-bit clean, therefore, developers cannot migrate older macs to new mode
15. fat binary was the dual mode programs
16. Virtual memory
    - uses the hard disk drive space to simulate memory
    - it needed the hardware support
    - divide the memory into blocks which is called pages
17. multithreading
    - allow an application to split itself into multiple threads
    - the application runs on multiple processors
    - the application uses multiple CPU
18. System 9
    - file sharing was modified to support the TCP/IP protocol
    - the older macs only supported the single user, but System 9 support for multiple users
    - new API
    - support for accelerated 3D rendering and OpenGL
19. OS X
    - PowerPC G3 processor
    
## Chapter 6
1. distribution
    - different distribution contain different libraries and utility programs
    - different distribution have different installation
    - different distribution have different purposes

2. Linux and Unix
    - Linux is free and it supports UNIX operations
    - Linux and Unix are multiuser system
2. Linux contain kernel and GNU system libraries and utility programs       
3. Linux kernel
    - monolithic       
    - Kernel is loaded into the program containing OS modules      
    - faster
    - OS code will run in the supervisor mode, so bug will cause big problem      
    - the source code is complex       
4. SUS is the standard for Linux
5. current production release is for standard installation, safe, stable          
6. current development release is not for standard installation, not safe, not stable 
7. top-half and bottom-half
    - if adapter received the packet it will generate interrupt to let OS stop and deal with the arriving packet
    - some packet are important and others are not 
    - some work needed to be done by kernel immediately and others are not
    - the top-half are things needed to be done immediately
    - the bottom-half are things not needed to be done immediately
    - bottom-half can run with only one CPU. bottom-half is redesigned to tasklet which can run with multiple CPUs
8. permissions change mode
    - chmod
    - arguments are a file name and a file mode
9. change group
    - chgrp
10. Linux is a preemptive multitasking system
    - OS will set a timer for the running process and if the process is running a long time the OS wil interrupt the process
    - the process cannot run forever to prevent higher process to run and the higher processes will be able to run
11. In Linux, processes and threads are tasks
12. fork process
    - a process used system call fork to start another process
    - the first process is the parent process
    - the second process is the child process
     -child process is the copy of the parent process
    - In Linux, it is clone
13. Multiprocessing system
    - allow multiple CPUs to run in a single system
    - single CPU system allow single program to run at the same time
    - multiple CPU system allow multiple program to run at the same time
    - multiple CPU system is more cost effective than making each individual CPU faster and faster
    - multiple CPU approaches        
        - asymmetric multiprocessing, OS runs on designated CPU and applications run on other CPU
        - symmetric multiprocessing, SMP, OS runs on any CPU
        - Linux support SMP
    
## Chapter 7      
1. Moore’s law says faster and faster. Why do we need to exploit parallelism in a design
    - CPU has limitation about what it can do in one clock, so CPU CPUs need to do work in parallel
    
2. How the symmetric multiprocessing, SMP, clusters, grid systems use CPU
    - symmetric multiprocessing, SMP, clusters share CPU and memory, a single OS that will run on any CPU, it will manage process scheduling for the CPU, memory allocation, and other resources
    - grid do not share CPU and memory
3. nodes in a cluster system
    - it is single processors or MP systems
    - it is managed by special software and connected via the separate dedicated LAN
    - it is administrated by single user, so the user name and password is the same for the node in the cluster
4.  Workflows contain pipeline flows and sweep flows
    - pipeline flows indicate dependencies
    - sweep flows will be done simultaneously in parallel
5. Uniprocessor allow parallel on the SMP
    - CPU preference 
    - processor affinity
6. Hardware for scheduling processes
    - locking mechanisms
7. cluster systems parallel
    - middleware
8. nodes in a grid system
    - it is administrated by different user, so the user name and password is the different for the node in the grid
    - grid run the same OS, but the multithreading, MPI, RMI, and middleware is not effective due to the loosely connection 
9. Uniprocessor use critical sections,it is based on shared memory and special CPU instructions, but cluster and grid do not use it
10. Handle failure
    - hardware will cause failure and software will cause failure
    - software is written to account for failure
    - monitoring system will watch network traffic to find failure

## Chapter 8 
1. The CPU state information is saved in the Process control block, PCB, when the OS stop the process. The CPU state information will not be stored when the process is running. The different OS store different information in the PCB.

2. “state” is overloaded
    - CPU state information is saved in PCB when the OS stop the process
    - process state information
3. OS states
    - new
        - create PCB
        - assign process
        - fill in PCB parameter
        - reserve memory
    - ready
        - the OS select it as the next process to run
        - it will be dispatched
    - wait
        - request a synchronous I/O operation
        - wait until the operation is complete
    - run
    - exit
4. why fair scheduler
    - starvation. It is possible that higher-priority processes keep running and let the lower-priority process have no chance to run. we should not let starvation problem happen.       
    - some OSs use priority scheduling, but others are not. In order to prevent the starvation problem from happening, the OS should provide process a fair share of CPU and provide process the reasonable amount of running time. Therefore, some process will not run forever, other process will have chance to run.      
5. first come, first served, FCFS, is easy to implement, is well understood, and is fair, but the problem is that the running process will have a bug or it will try to more CPU time to make itself have better user response
6. some process is much more important, so we do not wat to use FCFS, so we will use the priority scheduling
7. shortest runtime first gets the highest priority, SRTF
    - why not use it
        - computers don’t come with the “mind reader” option installed, so we usually don’t know how long the next CPU burst of a process will be
8. preemption, preemptive system
    - we know some process has higher priority than the running process, so we can stop the running process and start the higher priority process
9. Multilevel queue
    - a strict priority mechanism
    - share the CPU among the queues
10. why GUIs not use long-term scheduler
    - the OS will not automatically start all the process
    - the process will go into the queue and the long-term scheduler will decide how many process to run, what process to run, and when the process will run
    - when you click the icon, the process will run, when you click close, the process wil exit, so you only use it for short time
11. processor affinity is for the OS to indicate a preference for this process to run on some particular CPU whenever possible
12. the process start another process
    - Command Interpreter process will find a way to start the user’s process
    - Command Interpreter will use fork() to start another process. The parent process will make the fork() call and the child process will start running
13. fork(), exec(), wait(), system(), and shell
    - compare exec() and system()
        - same
            - create process
        - difference
            - exec() will call the OS kernel. system() will create a new shell
            - system() send the string to the new shell. fork() create a clone of the process. exec() clear it 
    - we will call fork(), then we will call exec() to load a new program into the child process
    - the parent process will elect continue to run or elect to use wait() call to go into wait state to wait until the child process is finished
    - we can use shell command to run another program. The shell command will create a new process but it will not copy the parent process to the child process
    
14. thread control block, TCB
    - store the CPU state in a table
15. threads and processes
    - we only have one copy of the program and data in memory, so we have more memory to use for other things
    - there is a much smaller overhead to switch between threads than to switch between processes since we are only saving the CPU state
    - process will have a difficult time communicating, but threads don’t have this problem
    - process is the running program. Threads is part of the process, it is created by process, and it is the lightweight process. If the thread is created by the same process, it will run in the same address space and share both the code and the dat
    - threads are easier to create and destroy than process
    - process is independent entities, it contains state information and address space. The OS divides application into process. Thread is the coding technique, so it will not have impact on the architecture of an application  
16. kernel-level threads is better than user-level threads
    - since the OS is aware of the individual threads, it is possible for the threads to execute on separate CPUs in a multi-CPU system. This is a major advantage for kernel-level thread
    - kernel threads are much easier to use than user thread
    - kernel-level threads can be scheduled preemptively, so a thread that takes too long to do its job cannot dominate the system
17. mapping the user threads to kernel thread
    - one-to-one 
        - When the application calls the library routine to create a new thread, the library routine calls the kernel thread mechanism to create a new kernel thread 
        - This model has the advantages of being fast and very simple to develop and for the user to understand
    - many-to-one 
        - Only one kernel thread is created and all the user threads will be mapped onto that same kernel thread
        - It is used only when an operating system has no kernel thread support
    - many-to-many
        - In this model the programmer will tell the system something about how many user threads and kernel threads will be needed and how they should be mapped
        - have multiple groups of user and kernel threads and to specify that some user threads are bound to a single kernel thread  
18. thread-safe
    - the library routine should be able to see the problem by always allocating local variables on the stack
19. Simultaneous multithreading
    - multiple processes send instructions to a single CPU at one time
20. POSIX thread drawback and downside
    - if the OS supports kernel threads, then the POSIX thread package is usually implemented using either the one-to-one or many-to-many models. This shows the downside of the POSIX thread standard when developing an applica- tion to run with the POSIX API
21. unique about Windows NT process scheduling
    - NT does implement processes, but it does not schedule processes. Instead, it implements a thread for every process, even if the application never indicates that it wants to use threads
    - NT schedules the threads instead of the processes
22. unique about Linux process scheduling
    - unix, linux multiprocess scheduler is Round Robin, the reason is that it supports multiple windows 
    - Linux supports the fork system call with the same effect as most UNIX systems, but it uses a memory management technique called copy-on-write to create the child task. This technique allows the child task to be created with very little overhead
23. Linux share file system
    - if the child and parent task do not share the same file system, then if the child task executes a chdir call, changing the current working directory, the current directory for the parent task will not change 
    - If the two tasks share the same file system then both tasks will see the change
24. Java threads
    - Java supports threads at the language level rather than through subroutine calls

## Chapter 9 
1. divide system into process
    - Performance, more economical to put in several inexpensive processors and run some portion of the process on each machine
    - Scaling, servers running the application
    - Purchased components, fit application into our system
    - Third-party service, use service from service
    - Components in multiple system
    - Reliability
    - Physical location of information
    - Enable application
    
2. Persistent and transient
    - persistent, the OS will retain messages that are intended for a process that is not running now or deliver messages from processes that are no longer running
    - transient, If one of the processes is unavailable then the system cannot function. Block data is persistent, it is used for caching
3. pipes, it is used for message exchange between processes
    - it is bidirectional
    - it is blocking communication mechanism
        - it has blocking type read
        - sending routine can put data into the buffer, it will be blocked
        - receiving routine can call a nonblocking system routine to read data
4. Persistent communication mechanism
    - message queuing is
    - Sockets is not, the benefit is that sockets has compatible implementation
5. shared memory
    - multiple processes running on the same machine, they are allowed to share memory
6. synchronization problems is hard to debug
    - it is intermittent
7. race condition
    - two processes trying to read and write the shared memory
    - when two or more threads trying to read and write the shared data and they try to change it at the same time
9. applications do not use spin-lock
    - does nothing but wait until the lock is not set when the instruction is executed
    - Swap is more powerful
    - they would waste valuable CPU cycles for an unknown amount of time
10. locking and unlocking system calls
    - Wait and signal
11. counting semaphores 
    - used for control access to a group of similar resources such as the records in a buffer
    - used for synchronize access to files where pro- cesses will do many reads and only a few writes
11. semaphores using test and set
    - if the value is 0, it will be able to use it. if the value is 1, it is in use 
12. Priority inversion describes a situation that can arise when a lower-priority task holds a shared resource that is required by a task running with a higher priority
13. block if the buffer is full. blocks if it finds the buffer is empty.        
14. monitor
    - in order to make locking and unlocking less error-prone
15. deadlock
    - what is it? 
        - processes are waiting to use resources that are using by other processes
    - where is it? 
        - the process will stop the same piece of information get updated at the same time. deadlock is for process, not the OS
    - when is it? 
        - you send something to printer and others want to use file system
    - conditions for deadlock
        - resources involved that are not sharable        
        - it is possible for a process to hold one resource while it waits for another
        - it tries to pick up the chopstick to the right, but finds that chopstick already in use, so it waits on the right chopstick
    - how to solve it? 
        - prevent it from happening
        - avoid  deadlocks by making sure that we do not allocate resources in such a way that they will ever happen
        - detect that if they have happened, and perhaps do something about them
    
## Chapter 10
1. why OS manage memory
    - we want to run programs that are larger than the primary memory of the machine and to allow as many programs to be running as possible
2. from the creation of a program to its execution in memory
    - write program
    - translate this symbolic program into a sequence of instructions
    - link that module with similar modules that were created separately
    - load the program into memory
    - Binding
3. what is the binding
    - Binding is the process of determining where in the physical memory the subroutine should go and making the reference in the main routine point to the subroutine
4. when is the binding
    - coding time
    - linking time
5. logical addressing space and a physical addressing space
    - logical address space is the set of addresses that the CPU would generate as the program executed
    - physical address space is the set of addresses used to load into the primary memory
6. Dynamic link libraries, DDLs
    - what it is, it is instructions, it has instruction for library function
    - it will save memory space, it exists in the memory only when it is loaded 
    - benefit 
        - the program is smaller so it take up less space on the disk drive and loads faster into RAM
        - only need one library module, so it saves disk space
        - only need to fix the bug in one of the library modules it is only necessary to fix the one library routing and load it onto the system
        - security fix
    - problem
        - When the developer of a software package is using a particular set of functions in a DLL, their code may also depend on bug fixes in a particular version of the library. They will want to make sure that the set of functions and bug fixes they are using is in the version of the library that is available on any system the package is installed on
        - dependence of earlier library that are subject to change
        
7. internal fragmentation
    - If we have set aside 100 KB for each partition and we want to run a program that only needs 6 KB, then we are wasting the rest of the space in that partition. This unused space is called internal fragmentation
    -  it is the unused space in the partition
8. external fragment
    - When a program ends the OS will again make that memory available to run another program
    - the free memory is not in one piece so we can’t run the program
    - the way to solve it, compaction, we found that allowing variable-sized programs to come and go in memory caused us to have external fragmentation of the memory and to spend valuable CPU time doing compaction
9. selecting the hole to use from among those large enough to run the process
    - first fit, the first suitable free partition
    - best fit, the smallest free partition which meets the requirement
    - worst fit, the largest available free portion
    - next fit, It begins as first fit to find a free partition. When called next time it starts searching from where it left off, not from the beginning
10. Dynamic loading and Dynamic link libraries
    - we do not load the entire program into primary memory at one time
    - put off even linking the subroutine with the main module
    
## Chapter 10
1. hardware development solved external fragmentation
    - we divide the memory into fixed-size blocks and instead of allocating to an application the entire space it needs in one large segment, we allocate enough of the smaller blocks to give the program what it needs
2. paging
    - the blocks we allocate do not need to be contiguous—they can be anywhere in memory because we ask the MMU to dynamically relocate each block separately
3. pages 
    - divide the logical addressing space into blocks
4. displacement
    - The displacement is the address of the specific byte within the frame. If our frame size is 4 KB then our displacement field is the exact size needed to address every byte in a frame, or 12 bits
    - When we relocate a logical address to a physical address we will still be addressing the same displacement in the frame as we were in the page. So we will not change the displacement part of the logical address
5. memory address control
    - use a fixed page table size
    - use a page table with a variable size
6. the page tables are sparse 
    - what is it, they may have large parts of the table that are in the logical address space but do not point to a valid fram
    - how to solve it, multilevel page table
7. segmentation, solve the same problems that paging addressed
    - it is the same thing as paging
    - the hardware to relocate all references to addresses in each segment in the same way it relocated ref- erences to the entire process with a relocation register
    - the logical address to be broken into two parts, but they will be a  segment number  ( s ) and a  displacement  ( d )
8. paging and segmentation
    - Paging is trans- parent to the running process. An application program that was created to run in an OS where the process is mapped into a single large partition could run unchanged on a system that used a paged memory architecture
    - Segmentation, on the other hand, requires that programs somehow be structured so that they are divided into logical parts with different address spaces
    - with the proper hardware design we can run a segmented program architecture in combination with a paged memory architecture
        - The first design originated with the Multics project.  1   In this design we will have a page table for each segment of a process rather than a single page table for the process
        - The second design is used in more modern systems. In this design there is still a segment table, but instead of pointing to separate page tables for each segment, the addresses in the segment table lie within a linear address space, which is then mapped into the physical memory in the same manner that a paged system works
9. The demand paging
    - The idea is that we slightly modify the meaning of the valid bit in the page table. The hardware associated with the use of the bit will not change. All the bit indicates is that there is no frame allocated for this page
10. demand paging, how does the OS know a page is needed by a process
    - When we load the first page we will set its valid bit to true to indicate that it is in memory. We will mark the valid bit of every other page to show that that page is not in memory. Then we will start the program running
11. The dirty pages 
    - the page have been changed since the page was brought into memory
    
## Chapter 11

1. every OS directory structure will include file metadata and inode.      
        
2. single-level directory structures problem        
    - the names were kept short      
    - the pointers to the blocks on the disk were kept small        
    - only a few files on the disk
3. tree hierarchical directory allow for the existence of multiple files with the same name      
    - keeping multiple files in separate directories
    - in order to specify the file name, we will have to give the entire  path  of the directories leading to the file
4. directory entry alias
    - An alias is an entry that does not point directly to a file, but rather points to another directory entry
    - With only a tree structured directory we are often in a quandary as to how we should classify some file
5. directories optimize searching time      
    - hashing
6. sequential access what happens to the current record pointer on a read
    - the OS will increment the current record pointer for each read or write
7. random access
    - In this model the application will tell the OS which record in the file it needs and the OS will move directly to that record and access it for reading or writing
8. raw access
    - reserves an area of the disk wherein the application can provide its own structure
9. keep track of free space in file systems
    - linked lists and bitmaps
10. file space allocation in file systems
    - Contiguous allocation
    - Linked allocation
    - Indexed allocation, it is convenient for random access files
                  
## Exercise
1.	Is google warehouse a grid?      
    -  It is grid. high-level sharing. Shared through network. Form virtual organizations.
      
2.	use all these cores simultaneously, what type of thread model could they use?
    - Multithreading. the application uses multiple CPU. the application runs on multiple processors. allow an application to split itself into multiple threads. 
3.	why are DDLs, dynamic link library used?
    - The bug is easy to be fixed. In order to modify the library, you will not recompile it. 
4.	Do overlays make program stacks, data, code, or heap smaller or larger?
    - It only overlay code. It will not make program stack and data smaller or larger.      
5.	Since multi-processing provides concurrency, what advantage do threads provide?
    - More efficient. More economical. more cost effective than making each individual CPU faster and faster.      
6.	Can multiple process programs also be multi threaded?
    - It is possible that the multiple processes or threads can be executed in parallel. 
7.	What is semaphore?
    - Semaphore is the variable used in the OS system calls. It has wait and signal routine.             
8.	When should semaphores be used?
    - It only support locking.        
9.	Where are they before they are swapped and where are they swapped to?
    - From the memory to the secondary storage.        
10.	Why are they swapped?
    - If memory is full, it will occur. It will save memory and we will run another program and keep the CPU busier. 
11. CPU scheduler
    - FCFS
    - SJN, no preemption        
    - SJN, preemption
    - Round Robin
    - Priority, no preemption
    - Priority, preemption
12. CPU scheduler compare
    - arrive time
    - run time
    - wait time
    - turnaround time
    - throughput
    - context switch for process
        - Round Robin scheduler have a lot context switch, which will have impact on the performance
        - if time quantum is large, Round Robin scheduler is more efficient. if time quantum is small, Round Robin scheduler is more reactive
13. For a CPU with 14 bit addresses, paged virtual memory is used, offsets (page offsets) are 8 bits, and there is 4 KB of real memory. 
    - read or write main memory is 1 * 10^-6 second
    - read or write a page to the swap flash is 2 * 10^-3 second
        - memory page is 2^8, 2^offsets byte
        - RAM is 4 KB 
        - addressable memory is 2^14, 2^addresses byte
        - entries is 2^6, 2^(14 - 8), 2^(addresses - offsets) byte
    - if the hit rate is 96%, what is the average memory access time
        - 0.96 * 10^-6 + (1 - 0.96) * (2 * 10^-3)       
    - One of the page replacement algorithms (policy) is FIFO
        - which page would be replaced, valid, so 1, earliest loaded, so small one
    - LRU
        - which page would be replaced, valid, so 1, earliest used, so small one 
    - when does page replacement happen
        - when you do not have place in main memory
    - We wish to decrease the average memory access time
        - Put page table in real memory
            - slower
        - Change reading swap disk pages to take 4 ms
            - compare 4 ms with read or write a page 2 ms, so slower
        - decrease real memory
            - slower
        - increase real memory
            - faster 
        - For page entry 2, the dirty bit is 1. How would that be used
            - the dirty shows that it has been changed. When you replace it, you should write it out
        - if it is valid, you cannot have 2 pages that have the same memory frame
        - if it is not valid, 2 pages may have the same memory frame
        - logical to physical
            - page entry -> memory frame
        - physical to logical
            - memory frame -> page entry      
14. Disk scheduling
    - [Disk Scheduling](lecture/Disk_Scheduling.pdf)
15. Disk capacity
    - [Disk Capacity](lecture/Disk_Capacity.png)  

    
    
    
         
      
   
        
     



                                                       
    
            
 


 





 
    
    
          





 




    
 
    



   
          
    

                    
 


 










