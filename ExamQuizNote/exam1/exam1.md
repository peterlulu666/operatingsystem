1. For each of the following, please give a short answer, (no more than 20 to 30 words)
    - When a process transitions from one state to another, does it also move from software to a controller?
        - No. It is the process that is reaching the end or causing problem.
    - While CP/M was single tasking (one process), Unix supported multitasking. But the Palm OS, for a single user also supported multitasking. Why?
        - CP/M is single tasking and the reason is that it supported only one program at a time.
        - Unix supported multiple users, so it supported multiple tasking.
        - Palm OS only support single user and it will only display one program on the screen, but it does not mean that it is the single tasking. Actually, Palm OS supported a few processes at the same time and it will run multiple applications at the same time. 
    - The Palm had both ROM and Non-volatile (flash) memory. Since it also had RAM, why did it need both ROM and flash memory?
        - The Palm needed to use RAM to store data. It used ROM to boot. Furthermore, if user is not using the Palm, the Palm will go to sleep mode and the data stored in the RAM will not be removed. If the user awake the Palm, the data is still there. What if the Palm is turned off or the battery is dead? The RAM will not be able to store data. So the ROM and the flash memory is there to help store data at this point. 

2. On a standard (size) CP/M disk, which has two sides, 128 bytes per sector, 77 tracks, and 26 sectors per track, we wish to store the following files, very briefly, for each case, explain why(show your calculations)
    - as many 200 KB files as possible. How many can we store?
        - 200 KB = 200000 Byte, So it is (77 * 26 * 128 * 2) / 200000 files we can store. 
    - as many 20 byte files as possible. How many can we store?
        - So it is (77 * 26 * 128 * 2) / 20 files we can store.
    - as many 1 Megabyte files as possible. How many can we store?
        - 1 Megabyte = 1000000 Byte. So it is (77 * 26 * 128 * 2) / 1000000 files we can store. 

3. What would increase the storage capacity of a CP/M disk more, increase sectors size to 512 bytes, or increase sectors per track to 100?
    - increase sectors size to 512 bytes. 

4. Some versions of MAC had pre-emptive multi-tasking, some did not. What is pre-emptive multi-tasking? How could you know which (it had it or not) in your MAC (without know which “version” you had)?
    - OS will set a timer for the running process and if the process is running a long time the OS will interrupt the process. Therefore, the process cannot run forever to prevent other process to run and other processes will be able to run. The user do not need to know that. 

5. What do each of the following C functions (methods) do? (From your assignment, Briefly explain what it does, if possible, or say “never used it”, if not used)
    - previous()
        - It is the user defined function. It will display the files in the previous page. 
    - system()
        - Create a shell. User will be able to enter the command into it and it will run the command. 
    - writedir()
        - It is the user defined function. 

6. In Unix how can a user check to see if a file is executable?
    - type `ls -l` into terminal. 

7. If you had a C program, written for CP/M, that calculated the sum of all the odd numbers from to 1001 to 10001, and printed the result “Odds Sum=”, followed by the result; Then you compiled and put program into memory, (There are about 5000 machine instructions. Make reasonable estimates for the rest) on CP/M, how would the memory layout look like? (What is in RAM, what in ROM, What addresses are used, what “order” in memory?)
    - The a.out file will be stored into the disk or the ROM. Instructions is in RAM. OS will occupy the highest locations in memory. The address will be used to load the program is the fixed address, let’s say that the 100 Hex. 

8. You want to write a program that runs a user’s program, named “myprog” with the parameter “1”. The user’s program is in a subdirectory called “prog” which is a subdirectory (folder) to the current directory. If the user’s program takes longer than 4 seconds to run, print “Too long”, otherwise print the time it took to run, in seconds.

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main() {
    char filename[256] = "./prog/myprog";
    clock_t start = clock();
    system(filename);
    clock_t finish = clock();
    double run_time = (double) (finish - start) / CLOCKS_PER_SEC;
    if (run_time > 4) {
        printf("Too long. \n");
    } else {
        printf("The run time is %f seconds. \n", run_time);
    }

    return 0;
}

```
Bonus: Rather than 4 seconds, make that time limit a user supplied value, AND “capture” the output of myprog in a file called “myprog.out”.





             

             