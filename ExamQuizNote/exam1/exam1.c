#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main() {
    char filename[256] = "./prog/myprog";
    clock_t start = clock();
    system(filename);
    clock_t finish = clock();
    double run_time = (double) (finish - start) / CLOCKS_PER_SEC;
    if (run_time > 4) {
        printf("Too long. \n");
    } else {
        printf("The run time is %f seconds. \n", run_time);
    }

    return 0;
}
