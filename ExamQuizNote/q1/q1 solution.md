1. For each of the following, briefly (no more than 20 to 30 words) explain:
   - (a) Is a process hardware, software, or neither? 
        - It is software. 
   - (b) What is a keyboard driver, what does it do? 
        - The software that connects controller and devide.
   - (c) Are disk controllers part of a microkernel? 
        - No. Disk controllers is hardware. Microkernel is software.  
2. In your assignment, there were several function calls, for EACH of the following,
   briefly describe what it does IF IT IS used in the assignment, AND IF it is a system
   call, write “system call”.
   - (a) time() 
        - system call
   - (b) pcb() 
        - user defined function
   - (c) ctime() 
        - it is the localtime
   - (d) system() 
        - create a shell
   - (e) writedir() 
        - user defined function
   - (f) previous() 
        - user defined function
3. If your shell (the one for assignment 1) is named (or renamed) to “shell.exe”, 
   and you call “system(“shell.exe”);”, what happens? 
   - It will run with no problem. 
4. In many ways a process and a device driver are similar. 
   - (a) Is a device driver a process (briefly, explain)? 
        - No. device driver is in kernel. Process is in user. 
   - (b) Is a process a device driver (briefly, explain)? 
        - No. process is running program. device driver is software. 

5. You want to write a program that asks a user to enter a file name (an existing file 
   in the current working directory) and a directory name (which may be as subdirectory 
   or any other directory) and you will try to move the file to that directory.
   (you may use “mv filename directoryname”). You should first check if the file 
   exists in the current working directory, and then check if it already exists 
   (same name) in the target directory. If it does not exist in the current directory,
   print “file doesnt exist” and exit. If it already exists in the target directory,
   print “file exists” and exit. Otherwise move the file.
   
   You can use:
     - struct stat buffer;
     - int exist = stat(filename,&buffer);  /* returns 0 if file exists */
     
 ```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>

int main() {
    // filename is yourfilename
    printf("Enter the file name \n");
    char filename[256];
    fgets(filename, 256, stdin);
    // Use strcspn to eat the newline
    filename[strcspn(filename, "\n")] = '\0';

    // directory is ./directory/
    printf("Enter the directory name \n");
    char directoryname[256];
    fgets(directoryname, 256, stdin);
    // Use strcspn to eat the newline
    directoryname[strcspn(directoryname, "\n")] = '\0';

    // file_in_directory is ./directory/yourfilename
    char file_in_directory[256];
    strcpy(file_in_directory, directoryname);
    strcat(file_in_directory, filename);

    struct stat buffer;
    int exist = stat(filename, &buffer);

    // returns 0 if file exists in the current directory
    if (exist == 0) {
        int exist_in_directory = stat(file_in_directory, &buffer);
        // returns 0 if file exists in the target directory
        if (exist_in_directory == 0) {
            printf("file exists \n");
            exit(0);
        } else {
            char cmd[256];
            strcpy(cmd, "mv ");
            strcat(cmd, filename);
            strcat(cmd, " ");
            strcat(cmd, directoryname);
            system(cmd);
        }

    } else {
        printf("file doesnt exist \n");
        exit(0);
    }

    return 0;

}
```


