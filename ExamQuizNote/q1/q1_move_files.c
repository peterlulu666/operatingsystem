#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>

int main() {
    // filename is yourfilename
    printf("Enter the file name \n");
    char filename[256];
    fgets(filename, 256, stdin);
    // Use strcspn to eat the newline
    filename[strcspn(filename, "\n")] = '\0';

    // directory is ./directory/
    printf("Enter the directory name \n");
    char directoryname[256];
    fgets(directoryname, 256, stdin);
    // Use strcspn to eat the newline
    directoryname[strcspn(directoryname, "\n")] = '\0';

    // file_in_directory is ./directory/yourfilename
    char file_in_directory[256];
    strcpy(file_in_directory, directoryname);
    strcat(file_in_directory, filename);

    struct stat buffer;
    int exist = stat(filename, &buffer);

    // returns 0 if file exists in the current directory
    if (exist == 0) {
        int exist_in_directory = stat(file_in_directory, &buffer);
        // returns 0 if file exists in the target directory
        if (exist_in_directory == 0) {
            printf("file exists \n");
            exit(0);
        } else {
            char cmd[256];
            strcpy(cmd, "mv ");
            strcat(cmd, filename);
            strcat(cmd, " ");
            strcat(cmd, directoryname);
            system(cmd);
        }

    } else {
        printf("file doesnt exist \n");
        exit(0);
    }

    return 0;

}
