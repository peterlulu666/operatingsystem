#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>


#define MAX_FOLDER_ARRAY_SIZE 1024

#define MAX_NAME_SIZE 2048

// Create the folder_array to store directory names
char folder_array[MAX_FOLDER_ARRAY_SIZE][MAX_NAME_SIZE];
// Count how many elements we will add to the folder_array in the current iteration
int folder_length = 0;

// Count how many elements we will add to the struct
int file_length = 0;

DIR *d;
struct dirent *de;
int i, c, k;
char s[MAX_NAME_SIZE], cmd[256];
time_t t;

// typedef struct
// The file info are sorted together
typedef struct fileInfo {
    char file_name[MAX_NAME_SIZE];
    int file_size;
    char file_permission[MAX_NAME_SIZE];
    int file_year;
    int file_month;
    int file_day;
    int file_hour;
    int file_minute;
    int file_second;
} FILEINFO;

// We will initialize the struct
FILEINFO file_information_init = {
        .file_name = "",
        .file_size = 0,
        .file_permission = "",
        .file_year = 0,
        .file_month = 0,
        .file_day = 0,
        .file_hour = 0,
        .file_minute = 0,
        .file_second = 0};

// We will use the & to get the address of the struct and copy the address to the pointer
FILEINFO *file_information = &file_information_init;

//// We would use this syntax to initialize the struct
//FILEINFO *file_information = &((FILEINFO) {
//        .file_name = "",
//        .file_size = 0,
//        .file_permission = "",
//        .file_year = 0,
//        .file_month = 0,
//        .file_day = 0,
//        .file_hour = 0,
//        .file_minute = 0,
//        .file_second = 0});

// Store January to December to the array
char month_array[12][256] = {
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"};

void add_directory_names();

void displaying_directories();

void add_file_information();

void displaying_files();

void displaying_main_menu();

char *check_number(char prompt[256], char file_or_directory[256], char *operation);

int cmp_date_time(const void *a, const void *b);

int cmp_size(const void *a, const void *b);

int main() {
    while (true) {
        // Print welcome
        printf("+-------------------Welcome-------------------+ \n");
        printf("|                                             | \n");
        printf("|          Welcome to myshell program         | \n");
        printf("|                                             | \n");
        printf("+---------------------------------------------+ \n");

        // Print time
        t = time(NULL);
        printf("====================TIME=======================\n");
        printf("           %s                                  \n", ctime(&t));


        // Print current working directory
        getcwd(s, MAX_NAME_SIZE);
        printf("===========CURRENT WORKING DIRECTORY===========\n");
        printf("%s                                             \n\n", s);

        // Add directory names to the array
        add_directory_names();

        // Print directories
        printf("=================DIRECTORIES===================\n");
        if (folder_length == 0) {
            printf("There is no subdirectory in the current directory. \n");
        }
        displaying_directories();

        printf("-----------------------------------------------\n");

        // Add file information to the struct
        add_file_information();

        // Print files info
        printf("====================FILES======================\n");
        if (file_length == 0) {
            printf("There is no fies in the current directory. \n");
        }
        displaying_files();

        printf("-----------------------------------------------\n");

        // Print the main menu
        displaying_main_menu();
    }

    return 0;

}

void add_directory_names() {
    // Store directory names to folder_array

    // Set the folder_length to 0
    // In every iteration, we will get started copying
    // directory names to folder_array at the index 0
    folder_length = 0;

    d = opendir(".");
    c = 0;
    struct stat folder_buffer;
    // File path
    char folder_path[256];
    // File name
    char folder_name[256];
    // Traverse directory

    // Check if it fails to open the directory
    if (d == NULL) {
        printf("It fails to open the directory. \n");
        exit(0);
    }

    while ((de = readdir(d))) {
        // Check if it is the directory
        if ((de->d_type) & DT_DIR) {
            // If it is the directory, append the directory name to the folder_array
            strcpy(folder_array[folder_length], de->d_name);
            folder_length++;
        }
    }

    folder_length = folder_length;

    // The closedir subroutine closes a directory stream
    // and frees the structure associated with the
    // DirectoryPointer parameter

    // If you do not do it,
    // it will cause memory leak and further attempts to open directories will fail
    closedir(d);

}

void displaying_directories() {
    int folder_index = 0;
    char folder_n_or_p[256];
    char folder_quit[256];
    while (folder_index < folder_length) {
        if (strcmp(folder_quit, "q") == 0 || strcmp(folder_quit, "Q") == 0) {
            break;
        }

        printf("           %d Directories: %s \n", folder_index, folder_array[folder_index]);
        folder_index++;

        // You can only show 5 lines of file names in one page
        // You will need to implement Prev, and Next Operations so that the menu fits
        // on one screen
        if (folder_index % 5 == 0) {
            printf("+---------------Directory menu----------------+ \n");
            printf("|                                             | \n");
            printf("|      Hit N for Next or hit P for prev       | \n");
            printf("|        If you do not want to display        | \n");
            printf("|    additional directories, press q to quit  | \n");
            printf("|           displaying directories            | \n");
            printf("+---------------------------------------------+ \n");
            printf("Enter your choice: ");
            fgets(folder_n_or_p, 256, stdin);
            // Use strcspn to eat the newline
            folder_n_or_p[strcspn(folder_n_or_p, "\n")] = '\0';
            strcpy(folder_quit, folder_n_or_p);

            if (strcmp(folder_quit, "q") == 0 || strcmp(folder_quit, "Q") == 0) {
                continue;
            }

            while (true) {
                if (strcmp(folder_n_or_p, "n") == 0 || strcmp(folder_n_or_p, "N") == 0) {
                    break;
                }
                if (strcmp(folder_n_or_p, "p") == 0 || strcmp(folder_n_or_p, "P") == 0) {
                    // User should not press p in the first page
                    // printf("           %d Directories: %s \n", folder_index,
                    // folder_array[folder_index]); is before folder_index++;
                    // Let's say that you are in the page 2,
                    // the file_index is 10, but the last file it will print is at file_index 9
                    // So file_index - 10 is 0, in the next iteration,
                    // it will get started printing at file_index 0, which is the first page
                    // It is the same for page 3, 4, 5, 6...
                    folder_index = folder_index - 10;

                    // Of course a user may type “P” ten times in succession, so
                    // you shouldn’t try to move the “window” of files
                    // you are showing to “before” the first

                    // If the user press p in the first page
                    // it will tell the user that this is the first page and
                    // it will display files in the first page
                    // so if the file_index is < 0, we need to set it to 0,
                    // in the next iteration, it will get started
                    // printing at file_index 0, which is the first page
                    if (folder_index <= 0) {
                        printf("+--------------Oops, that's wrong-------------+ \n");
                        printf("|                                             | \n");
                        printf("|     It would not display any page before    | \n");
                        printf("|                the first page               | \n");
                        printf("+---------------------------------------------+ \n");
                        folder_index = 0;
                    }
                    break;
                }

                // If the user did not choose N or P,
                // let the user know that they need to choose N or P
                printf("+--------------Oops, that's wrong-------------+ \n");
                printf("|                                             | \n");
                printf("|        You should choose N or P             | \n");
                printf("|      Hit N for Next or hit P for prev       | \n");
                printf("|        If you do not want to display        | \n");
                printf("|    additional directories, press q to quit  | \n");
                printf("|           displaying directories            | \n");
                printf("+---------------------------------------------+ \n");
                printf("Enter your choice: ");
                fgets(folder_n_or_p, 256, stdin);
                // Use strcspn to eat the newline
                folder_n_or_p[strcspn(folder_n_or_p, "\n")] = '\0';
                strcpy(folder_quit, folder_n_or_p);
                if (strcmp(folder_quit, "q") == 0 || strcmp(folder_quit, "Q") == 0) {
                    break;
                }
            }
        }
    }
}

void add_file_information() {
    // Add elements to the struct

    // Set the file_length to 0
    // In every iteration, we will get started copying
    // file names to to the struct at the index 0
    // If we use change directory, move file to directory or remove file,
    // the number of files will change, let's say that there is 20 files, we remove 10 files,
    // in the next iteration, we will only add 10 file names to
    // the struct from index 0 to index 9.
    // However, there are still file names in the struct
    // from index 10 to index 19 and we do not need them.
    // Do we need to delete them? No.
    // When printing the struct, we only print elements from index 0 to
    // struct length, which is 10.
    // When we use edit or run operation, we will check if the user enters a number that is
    // greater than 0 and less than struct length, which is 10.
    file_length = 0;

    d = opendir(".");
    c = 0;
    FILE *fp;
    // File size
    int len = 0;
    struct stat buffer;
    // File path
    char file_path[256];
    // File name
    char file_name[256];
    // File time and date
    char file_time_and_date_str[256] = "";
    // File permission
    char file_permission[256] = "";

    // Check if it fails to open the directory
    if (d == NULL) {
        printf("It fails to open the directory. \n");
        exit(0);
    }

    // Traverse directory
    while ((de = readdir(d))) {
        if (((de->d_type) & DT_REG)) {
            // If it is the regular file, append the file name to the struct
            strcpy(file_information[file_length].file_name, de->d_name);

            // Show additional file information, in addition to file name, show file size,
            // time and date of creation, R/W/X (read/write/execute permissions),
            // and similar information

            // In order to show file size, time and date of creation, R/W/X
            // First, we need to get the size of file in bytes and
            // append the integer to the struct
            // Second, we need to get the time and date of creation of file and
            // append the string to the struct
            // Third, we need to get the permission of file and
            // append the string to the struct
            strcpy(file_path, "./");
            strcpy(file_name, de->d_name);
            strcat(file_path, file_name);
            if (!stat(file_path, &buffer)) {
                // If you look at the man page 2 stat, you will find struct stat field
                // st_size: file size, in bytes
                /*This code was taken from the website
                 * Source code website:
                 * https://stackoverflow.com/questions/8236/how-do-you-determine-the-size-of-a-file-in-c
                 * Source code:
                 *  #include <sys/stat.h>
                    off_t fsize(char *file) {
                        struct stat filestat;
                        if (stat(file, &filestat) == 0) {
                            return filestat.st_size;
                        }
                        return 0;
                    }
                 *
                 * What I have learned:
                 * I learned how to use the stat() system call to get file size
                 * */
                // Append file size to the struct
                file_information[file_length].file_size = buffer.st_size;

                // If you look at the man page 2 stat, you will find the time-related fields of struct stat
                // st_atime: Time when file data last accessed
                // st_mtime: Time when file data last modified
                // st_ctime: Time when file status was last changed
                // st_birthtime: Time of file creation
                // We need to use st_birthtime for the time and date of creation
                /*This code was taken from the website
                 * Source code website:
                 * https://www.tutorialspoint.com/c_standard_library/c_function_strftime.htm
                 * Source code:
                       strftime(buffer,80,"%x - %I:%M%p", info);
                 *
                 * What I have learned:
                 * I learned how to use the strftime() function to format time
                 *
                 * Source code website:
                 * https://www.daniweb.com/programming/software-development/threads/25812/how-to-get-last-accessed-created-modified-file-date-and-time
                 * Source code:
                 *  #include <time.h>
                    #include <sys/types.h>
                    #include <sys/stat.h>
                    #include <stdio.h>
                    int main()
                    {
                        char filename[] = "c:\\test.txt";
                        char timeStr[ 100 ] = "";
                        struct stat buf;
                        time_t ltime;
                        char datebuf [9];
                        char timebuf [9];
                        if (!stat(filename, &buf))
                        {
                            strftime(timeStr, 100, "%d-%m-%Y %H:%M:%S", localtime( &buf.st_mtime));
                            printf("\nLast modified date and time = %s\n", timeStr);
                 *
                 * What I have learned:
                 * I learned how to use the strftime() function to format time
                 * */
                // buffer.st_birthtime is the long type, we will convert long to string
                // We will use strftime() to format time
                // The specifier we will use is
                // %x: Date representation, for example, 08/19/12
                // %I: Hour in 12h format, for example, 05
                // %M: Minute, for example, 55
                // %p AM or PM designation, for example, PM
                strftime(file_time_and_date_str, 256, "%Y-%m-%d %H:%M:%S", localtime(&(buffer.st_birthtime)));

                // Add year, month, day, hour, minute, and second to struct

//                    // Print substring
//                    printf("%.*s\n", 2, date_str + 0);

                // Store substring

                // Substring from index 0 to index 3
                char *year_str = malloc(4);
                strncpy(year_str, file_time_and_date_str + 0, 4);
                int year_integer = atoi(year_str);
                file_information[file_length].file_year = year_integer;

                // Substring from index 5 to index 6
                char *month_str = malloc(2);
                strncpy(month_str, file_time_and_date_str + 5, 2);
                int month_integer = atoi(month_str);
                file_information[file_length].file_month = month_integer;

                // Substring from index 8 to index 9
                char *day_str = malloc(2);
                strncpy(day_str, file_time_and_date_str + 8, 2);
                int day_integer = atoi(day_str);
                file_information[file_length].file_day = day_integer;

                // Substring from index 11 to index 12
                char *hour_str = malloc(2);
                strncpy(hour_str, file_time_and_date_str + 11, 2);
                int hour_integer = atoi(hour_str);
                file_information[file_length].file_hour = hour_integer;

                // Substring from index 14 to index 15
                char *minute_str = malloc(2);
                strncpy(minute_str, file_time_and_date_str + 14, 2);
                int minute_integer = atoi(minute_str);
                file_information[file_length].file_minute = minute_integer;

                // Substring from index 17 to index 18
                char *second_str = malloc(2);
                strncpy(second_str, file_time_and_date_str + 17, 2);
                int second_integer = atoi(second_str);
                file_information[file_length].file_second = second_integer;

                // If you look at the man page 2 stat, you will find the file mode bits
                // S_IRUSR: read permission, owner
                // S_IWUSR: write permission, owner
                // S_IXUSR: execute/search permission, owner
                // S_IRGRP: read permission, group
                // S_IWGRP: write permission, group
                // S_IXGRP: execute/search permission, group
                // S_IROTH: read permission, others
                // S_IWOTH: write permission, others
                // S_IXOTH: execute/search permission, others
                /*This code was taken from the website
                 * Source code website:
                 * https://stackoverflow.com/questions/10323060/printing-file-permissions-like-ls-l-using-stat2-in-c
                 * Source code:
                 *  #include <unistd.h>
                    #include <stdio.h>
                    #include <sys/stat.h>
                    #include <sys/types.h>

                    int main(int argc, char **argv)
                    {
                        if(argc != 2)
                            return 1;

                        struct stat fileStat;
                        if(stat(argv[1], &fileStat) < 0)
                            return 1;

                        printf("Information for %s\n", argv[1]);
                        printf("---------------------------\n");
                        printf("File Size: \t\t%d bytes\n", fileStat.st_size);
                        printf("Number of Links: \t%d\n", fileStat.st_nlink);
                        printf("File inode: \t\t%d\n", fileStat.st_ino);

                        printf("File Permissions: \t");
                        printf( (S_ISDIR(fileStat.st_mode)) ? "d" : "-");
                        printf( (fileStat.st_mode & S_IRUSR) ? "r" : "-");
                        printf( (fileStat.st_mode & S_IWUSR) ? "w" : "-");
                        printf( (fileStat.st_mode & S_IXUSR) ? "x" : "-");
                        printf( (fileStat.st_mode & S_IRGRP) ? "r" : "-");
                        printf( (fileStat.st_mode & S_IWGRP) ? "w" : "-");
                        printf( (fileStat.st_mode & S_IXGRP) ? "x" : "-");
                        printf( (fileStat.st_mode & S_IROTH) ? "r" : "-");
                        printf( (fileStat.st_mode & S_IWOTH) ? "w" : "-");
                        printf( (fileStat.st_mode & S_IXOTH) ? "x" : "-");
                        printf("\n\n");

                        printf("The file %s a symbolic link\n", (S_ISLNK(fileStat.st_mode)) ? "is" : "is not");

                    What I have learned:
                    I learned how to print file permission in c language
                 * */
                strcpy(file_permission, "");
                strcat(file_permission, (buffer.st_mode & S_IRUSR) ? "r" : "-");
                strcat(file_permission, (buffer.st_mode & S_IWUSR) ? "w" : "-");
                strcat(file_permission, (buffer.st_mode & S_IXUSR) ? "x" : "-");
                strcat(file_permission, (buffer.st_mode & S_IRGRP) ? "r" : "-");
                strcat(file_permission, (buffer.st_mode & S_IWGRP) ? "w" : "-");
                strcat(file_permission, (buffer.st_mode & S_IXGRP) ? "x" : "-");
                strcat(file_permission, (buffer.st_mode & S_IROTH) ? "r" : "-");
                strcat(file_permission, (buffer.st_mode & S_IWOTH) ? "w" : "-");
                strcat(file_permission, (buffer.st_mode & S_IXOTH) ? "x" : "-");
                // Append file_permission to the struct
                strcpy(file_information[file_length].file_permission, file_permission);
            }
            file_length++;
        }
    }

    closedir(d);

}

void displaying_files() {
    int file_index = 0;
    char file_n_or_p[256];
    char file_quit[256];
    while (file_index < file_length) {
        if (strcmp(file_quit, "q") == 0 || strcmp(file_quit, "Q") == 0) {
            break;
        }

        char print_January_to_December[256] = "";
        for (int j = 0; j < 12; ++j) {
            // date_sorted_file_index is starting at 1
            // j is starting at 0
            // when j = 0
            // j + 1 = 0 + 1 = 1 is the first month
            // month_array[j] is month_array[0], it is the first month, it is January
            if (file_information[file_index].file_month == j + 1) {
                strcpy(print_January_to_December, month_array[j]);
            }
        }

        printf("           %d\t", file_index);
        printf("File Name: \t\t\t\t\t%s \n "
               "\t\tFile Size: \t\t\t\t\t%d bytes \n "
               "\t\tFile Permission: \t\t\t\t%s \n "
               "\t\tFile Time and Date of Creation: \t\t%d-%s-%d %d:%d:%d \n\n",
               file_information[file_index].file_name,
               file_information[file_index].file_size,
               file_information[file_index].file_permission,
               file_information[file_index].file_year,
               print_January_to_December,
               file_information[file_index].file_day,
               file_information[file_index].file_hour,
               file_information[file_index].file_minute,
               file_information[file_index].file_second);
        file_index++;

        // You can only show 5 lines of file names in one page
        // You will need to implement Prev, and Next Operations so that the menu fits
        // on one screen
        if (file_index % 5 == 0) {
            printf("+-----------------File menu-------------------+ \n");
            printf("|                                             | \n");
            printf("|      Hit N for Next or hit P for prev       | \n");
            printf("|        If you do not want to display        | \n");
            printf("|      additional files, press q to quit      | \n");
            printf("|             displaying files                | \n");
            printf("+---------------------------------------------+ \n");
            printf("Enter your choice: ");
//                printf("Hit N for Next or hit P for prev. "
//                       "If you do not want to display additional files, "
//                       "press q to quit displaying files \n");
            fgets(file_n_or_p, 256, stdin);
            // Use strcspn to eat the newline
            file_n_or_p[strcspn(file_n_or_p, "\n")] = '\0';
            strcpy(file_quit, file_n_or_p);

            if (strcmp(file_quit, "q") == 0 || strcmp(file_quit, "Q") == 0) {
                continue;
            }

            while (true) {
                if (strcmp(file_n_or_p, "n") == 0 || strcmp(file_n_or_p, "N") == 0) {
                    break;
                }
                if (strcmp(file_n_or_p, "p") == 0 || strcmp(file_n_or_p, "P") == 0) {
                    // User should not press p in the first page
                    // printf("           %d Directories: %s \n", folder_index,
                    // folder_array[folder_index]); is before folder_index++;
                    // Let's say that you are in the page 2,
                    // the file_index is 10, but the last file it will print is at file_index 9
                    // So file_index - 10 is 0, in the next iteration,
                    // it will get started printing at file_index 0, which is the first page
                    // It is the same for page 3, 4, 5, 6...
                    file_index = file_index - 10;

                    // Of course a user may type “P” ten times in succession, so
                    // you shouldn’t try to move the “window” of files
                    // you are showing to “before” the first

                    // If the user press p in the first page
                    // it will tell the user that this is the first page and
                    // it will display files in the first page
                    // so if the file_index is < 0, we need to set it to 0,
                    // in the next iteration, it will get started
                    // printing at file_index 0, which is the first page
                    if (file_index < 0) {
                        printf("+--------------Oops, that's wrong-------------+ \n");
                        printf("|                                             | \n");
                        printf("|     It would not display any page before    | \n");
                        printf("|                the first page               | \n");
                        printf("+---------------------------------------------+ \n");
//                            printf("You reach the first page. There is no file before the file 0. "
//                                   "So it would not display any page before the first page. \n");
                        file_index = 0;
                    }
                    break;
                }

                // If the user did not choose N or P,
                // let the user know that they need to choose N or P
                printf("+--------------Oops, that's wrong-------------+ \n");
                printf("|                                             | \n");
                printf("|        You should choose N or P             | \n");
                printf("|      Hit N for Next or hit P for prev       | \n");
                printf("|        If you do not want to display        | \n");
                printf("|      additional files, press q to quit      | \n");
                printf("|             displaying files                | \n");
                printf("+---------------------------------------------+ \n");
                printf("Enter your choice: ");
//                    printf("You should choose N or P. "
//                           "Hit N for Next or hit P for prev. "
//                           "If you do not want to display additional files, "
//                           "press q to quit displaying files \n");
                fgets(file_n_or_p, 256, stdin);
                // Use strcspn to eat the newline
                file_n_or_p[strcspn(file_n_or_p, "\n")] = '\0';
                strcpy(file_quit, file_n_or_p);
                if (strcmp(file_quit, "q") == 0 || strcmp(file_quit, "Q") == 0) {
                    break;
                }
            }
        }
    }
}

void displaying_main_menu() {
    printf("-----------------------------------------------\n");
    printf("                    MAIN MENU \n");
    printf("-----------------------------------------------\n");

    printf("Operation: D  Display \n"
           "           E  Edit \n"
           "           R  Run \n"
           "           C  Change Directory \n"
           "           S  Sort Directory listing \n"
           "           M  Move to Directory \n"
           "           X  Remove File \n"
           "           Q  Quit \n");
    printf("Enter your choice: ");
    // The permission denied happens a lot,
    // so we need to change the default permission
    // Set it to be readable, writable and executable by all users
    char *cmd_chmod = "chmod 777 *";
    char main_menu[256];
    fgets(main_menu, 256, stdin);
    // Use strcspn to eat the newline
    main_menu[strcspn(main_menu, "\n")] = '\0';

    // If the user press d, it will display directories and files
    if (strcmp(main_menu, "d") == 0 || strcmp(main_menu, "D") == 0) {
        return;
    }

        // If the user press e, it will open a text editor with a file
    else if (strcmp(main_menu, "e") == 0 || strcmp(main_menu, "E") == 0) {
        // If there is no file in the current directory,
        // it will not open a text editor
        if (file_length == 0) {
            printf("There is no file in the current directory. "
                   "So it will not edit file. \n");
            return;
        }

        // If there is file in the current directory,
        // it will let the user choose which file to edit
        char edit[256];
        char prompt[256] = "Edit what";
        char file_or_directory[256] = "file";
        // Check number
        strcpy(edit, check_number(prompt, file_or_directory, edit));
        // Edit the file
        strcpy(cmd, "pico ");
        // Create quotation marks around the name
        strcat(cmd, "\"");
        strcat(cmd, file_information[atoi(edit)].file_name);
        // Create quotation marks around the name
        strcat(cmd, "\"");
        system(cmd_chmod);
        system(cmd);
        return;
    }

        // If the user press r, it will run an executable program
    else if (strcmp(main_menu, "r") == 0 || strcmp(main_menu, "R") == 0) {
        // If there is no file in the current directory,
        // it will let the user choose which file to run
        if (file_length == 0) {
            printf("There is no file in the current directory. "
                   "So it will not run an executable program. \n");
            return;
        }

        // If there is file in the current directory,
        // it will let the user choose which file to run
        char run[256];
        char prompt[256] = "Run what";
        char file_or_directory[256] = "file";
        // Check number
        strcpy(run, check_number(prompt, file_or_directory, run));
        // Run the file
        strcpy(cmd, "./");
        // Create quotation marks around the name
        strcat(cmd, "\"");
        strcat(cmd, file_information[atoi(run)].file_name);
        // Create quotation marks around the name
        strcat(cmd, "\"");
        system(cmd_chmod);
        system(cmd);
        return;
    }

        // If the user press c, it will change directory
    else if (strcmp(main_menu, "c") == 0 || strcmp(main_menu, "C") == 0) {
        // Check if the folder_array is empty
        if (folder_length == 0) {
            printf("There is no directory in the current directory. "
                   "So it will not change directory. \n");
            return;
        }

        // If there is directory in the current directory,
        // it will let the user choose which directory to change to
        char change[256];
        char prompt[256] = "Change to";
        char file_or_directory[256] = "directory";
        // Check number
        strcpy(change, check_number(prompt, file_or_directory, change));
        system(cmd_chmod);
        // Change to directory
        strcpy(cmd, folder_array[atoi(change)]);
        if (chdir(cmd) == -1) {
            printf("%s : No such directory.\n", cmd);
        }
        return;
    }

        // If the user press s, it will sort the file
    else if (strcmp(main_menu, "s") == 0 || strcmp(main_menu, "S") == 0) {
        char sort[256];
        printf("Press s for sort by size or press d for sort by date. \n");
        fgets(sort, 256, stdin);
        // Use strcspn to eat the newline
        sort[strcspn(sort, "\n")] = '\0';

        if (strcmp(sort, "s") == 0 || strcmp(sort, "S") == 0) {
            // Use qsort to sort size
            qsort(file_information, file_length, sizeof(*file_information), cmp_size);

            // Print sorted file in descending order
            printf("=============SORTED FILES BY SIZE==============\n");
            printf("      The file size is in descending order \n");
            displaying_files();
        }

            // We will use qsort to sort date
        else if (strcmp(sort, "d") == 0 || strcmp(sort, "D") == 0) {

            qsort(file_information, file_length, sizeof(*file_information), cmp_date_time);

            // Print sorted file in chronological order
            printf("=============SORTED FILES BY DATE==============\n");
            printf("      The file date is in chronological order \n");
            displaying_files();
        }

        return;

    }

        // If the user press m, it will move file to directory
    else if (strcmp(main_menu, "m") == 0 || strcmp(main_menu, "M") == 0) {
        // If there is no file in the current directory,
        // it will not move file to directory
        if (file_length == 0) {
            printf("There is no file in the current directory. "
                   "So it will not move file to directory. \n");
            return;
        }

        // If there is file in the current directory,
        // it will let the user choose move what file to which directory
        char move_what[256];
        char prompt[256] = "Move what";
        char file_or_directory[256] = "file";
        // Check number
        strcpy(move_what, check_number(prompt, file_or_directory, move_what));
        // Move the file
        strcpy(cmd, "mv ");
        // Create quotation marks around the name
        strcat(cmd, "\"");
        strcat(cmd, file_information[atoi(move_what)].file_name);
        // Create quotation marks around the name
        strcat(cmd, "\"");

        // Check if the folder_array is empty
        if (folder_length == 0) {
            printf("There is no directory in the current directory. "
                   "So it will not change directory. \n");
            return;
        }

        // If there is directory in the current directory,
        // it will let the user choose which directory to change to
        char to_which_directory[256];
        char prompt_folder[256] = "Move to which directory";
        char file_or_directory_folder[256] = "directory";
        // Check number
        strcpy(to_which_directory, check_number(prompt_folder, file_or_directory_folder, to_which_directory));
        // Move file to which directory
        strcat(cmd, " ");
        // Create quotation marks around the name
        strcat(cmd, "\"");
        strcat(cmd, folder_array[atoi(to_which_directory)]);
        // Create quotation marks around the name
        strcat(cmd, "\"");
        system(cmd_chmod);
        system(cmd);
        return;
    }

        // If the user press x, it will remove the file
    else if (strcmp(main_menu, "x") == 0 || strcmp(main_menu, "X") == 0) {
        // If there is no file in the current directory,
        // it will not remove the file
        if (file_length == 0) {
            printf("There is no file in the current directory. "
                   "So it will not remove the file. \n");
            return;
        }

        // If there is file in the current directory,
        // it will let the user choose which file to remove
        char remove[256];
        char prompt[256] = "Remove what";
        char file_or_directory[256] = "file";
        // Check number
        strcpy(remove, check_number(prompt, file_or_directory, remove));
        // Remove the file
        strcpy(cmd, "rm ");
        // Create quotation marks around the name
        strcat(cmd, "\"");
        strcat(cmd, file_information[atoi(remove)].file_name);
        // Create quotation marks around the name
        strcat(cmd, "\"");
        system(cmd_chmod);
        system(cmd);
        return;
    }

        // If the user press q, it will quit
    else if (strcmp(main_menu, "q") == 0 || strcmp(main_menu, "Q") == 0) {
        printf("+---------------------------Thank you----------------------------+\n");
        printf("|                                                                |\n");
        printf("|          Thank you for your interest in the myshell program    |\n");
        printf("|                                                                |\n");
        printf("+----------------------------------------------------------------+\n");
        exit(0);
    }

        // If the user press other characters
    else {
        printf("We do not have the operation. \n");
        return;
    }
}

char *check_number(char prompt[256], char file_or_directory[256], char *operation) {
    while (true) {
        operation = malloc(256);
        printf("%s? Choose a %s number \n", prompt, file_or_directory);
        fgets(operation, 256, stdin);
        // Use strcspn to eat the newline
        operation[strcspn(operation, "\n")] = '\0';

        // Check if the user input is numeric
        // Check if character in edit only contains number from 0 to 9
        bool status = true;
        for (int j = 0; j < strlen(operation); ++j) {
            if (operation[j] != '0' &&
                operation[j] != '1' &&
                operation[j] != '2' &&
                operation[j] != '3' &&
                operation[j] != '4' &&
                operation[j] != '5' &&
                operation[j] != '6' &&
                operation[j] != '7' &&
                operation[j] != '8' &&
                operation[j] != '9') {
                status = false;
            }
        }
        if (status == false) {
            printf("Your input is not a number or it is a negative number. \n");
            continue;
        }

        // Check if the input number with two or more digits is starting with 0
        if (operation[0] == '0' && strlen(operation) > 1) {
            printf("Your input is a number, "
                   "but it is starting with 0. \n");
            continue;
        }

        // Check if the user input number is greater than the file_length
        if (atoi(operation) >= file_length) {
            printf("Your input is a number, "
                   "but it is greater than the number of files. \n");
            continue;
        }
        // If the user input is number and it is less than the total number of the files
        break;
    }

    return operation;

}

/*This code was taken from the website
 * Source code website:
 * https://www.daniweb.com/programming/software-development/threads/405653/qsort-an-array-of-structures-by-2-criteria
 * Source code:
 *  #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    struct record {
        char name[32];
        double value;
    };
    int compare(const void *a, const void *b)
    {
        const struct record *pa = a;
        const struct record *pb = b;

        int diff = strcmp(pa->name, pb->name);

        if (diff == 0) {
            if (pa->value < pb->value)
                diff = -1;
            else if (pa->value > pb->value)
                diff = +1;
            else
                diff = 0;
        }

        return diff;
    }
    int main(void)
    {
        struct record records[] = {
            {"A", 1},
            {"B", 13},
            {"B", 1},
            {"B", 2},
            {"C", 3},
            {"C", 1}
        };
        int size = 6;
        int i;

        qsort(records, size, sizeof(*records), compare);

        for (i = 0; i < 6; i++)
            printf("%s %f\n", records[i].name, records[i].value);

        return 0;
    }
 *
 * What I have learned:
 * I learned how to use qsort to sort dates chronologically
 * */
// cmp_year_month sorts year and month
int cmp_year_month(const void *a, const void *b) {
    const struct fileInfo *ptr_a = a;
    const struct fileInfo *ptr_b = b;

    int diff_year = ptr_a->file_year - ptr_b->file_year;

    if (diff_year == 0) {
        if (ptr_a->file_month < ptr_b->file_month)
            diff_year = -1;
        else if (ptr_a->file_month > ptr_b->file_month)
            diff_year = +1;
        else
            diff_year = 0;
    }

    return diff_year;

}

// cmp_date_time sorts year, month, day, hour, minute and second
int cmp_date_time(const void *a, const void *b) {
    const FILEINFO *ptr_a = a;
    const FILEINFO *ptr_b = b;

    int diff_year = ptr_a->file_year - ptr_b->file_year;

    // If the years are the same
    if (diff_year == 0) {
        // compare month
        if (ptr_a->file_month < ptr_b->file_month)
            diff_year = -1;
        else if (ptr_a->file_month > ptr_b->file_month)
            diff_year = 1;
            // If the months are the same
        else {
            // compare days
            if (ptr_a->file_day < ptr_b->file_day)
                diff_year = -1;
            else if (ptr_a->file_day > ptr_b->file_day)
                diff_year = 1;
                // If the days are the same
            else {
                // compare hours
                if (ptr_a->file_hour < ptr_b->file_hour)
                    diff_year = -1;
                else if (ptr_a->file_hour > ptr_b->file_hour)
                    diff_year = 1;
                    // If the hours are the same
                else {
                    // compare minutes
                    if (ptr_a->file_minute < ptr_b->file_minute)
                        diff_year = -1;
                    else if (ptr_a->file_minute > ptr_b->file_minute)
                        diff_year = 1;
                        // If the minutes are the same
                    else {
                        // compare seconds
                        if (ptr_a->file_second < ptr_b->file_second)
                            diff_year = -1;
                        else if (ptr_a->file_second > ptr_b->file_second)
                            diff_year = 1;
                        else
                            diff_year = 0;
                    }
                }
            }
        }
    }

    return diff_year;

}

// cmp_size sorts size
int cmp_size(const void *a, const void *b) {
    const FILEINFO *ptr_a = a;
    const FILEINFO *ptr_b = b;

    return ptr_b->file_size - ptr_a->file_size;

}
