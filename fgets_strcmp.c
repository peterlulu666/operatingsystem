#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    char n[100];
    int i;
    printf("Enter: \n");
    fgets(n, 100, stdin);
    printf("%s \n", n);
    n[strcspn(n, "\n")] = '\0';
    printf("%s \n", n);
    printf("%d \n", atoi(n));
    if (strcmp(n, "n") == 0) {
        printf("good \n");
    }

    return 0;
}