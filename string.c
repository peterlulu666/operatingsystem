#include <stdio.h>

int main() {
    char str[] = "computer";
    char strArray[] = {'a', 'b', 'c'};
    char *strPtr;
    strPtr = "test";
    char *strPointer[] = {"ls", "-l"};
    printf("%s\n", str);
    printf("%s\n", strArray);
    printf("%s\n", strPtr);
    printf("%s\n", strPointer[1]);
    printf("%.*s\n", 3, str + 2);
    return 0;
}