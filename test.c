#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <dirent.h>
#include <sys/stat.h>


int main() {
    struct dirent *de;
    struct stat buffer;
    DIR *d;
    d = opendir(".");
    char filePath[256] = "./";
    while ((de = readdir(d))) {
        if (!stat(filePath, &buffer)) {
            if (S_ISDIR(buffer.st_mode)) {
                printf("%s \n", de->d_name);
            }
        }
    }

    return 0;
}