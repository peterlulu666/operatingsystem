# Programming Assignment 1

## Description

In this assignment you will write your own shell program, Mav shell (msh), similar to bourne shell (bash), c-shell (csh), or korn shell (ksh). It will accept commands, fork a child process and execute those commands. The shell, like csh or bash, will run and accept commands until the user exits the shell. Your file must be named msh.c


## Administrative
1. program file name msh.c
2. programming language is C
3. compile and grade the program on [omega.uta.edu](omega.uta.edu)
4. code compiles on omega with `gcc -Wall msh.c -o msh —std=c99`
5. you can use code in the course github repository at: [https://github.com/CSE3320/Shell-Assignmen](https://github.com/CSE3320/Shell-Assignmen)

## Hints
1. `fork` `exec` `wait`
        - Use `fork` and one of the `exec` family as discussed in class to execute the command and call `wait` to wait for the child to complete. If the command is “cd” then use `chdir()` instead of `exec`. Note, chdir() must be called from the parent.        


## Functional Requirements

Requirement 1: Your program will print out a prompt of `msh>` when it is ready to accept input. It must read a line of input and, if the command given is a supported shell command, it shall execute the command and display the output of the command

```
[bakker@omega msh_ solution]$ ./msh 
msh> ls 
hello hello.c Loop Loop.c Makefile msh msh.c msh.o         
msh> 

```

Requirement 2: If the command is not supported your shell shall print the invalid command followed by `: Command not found.`


```
[bakker@omega msh_ solution]$ ./msh 
msh> ls 
hello hello.c Loop Loop.c Makefile msh msh.c msh.o         
msh> lss
lss: Command not found.         

```        

Requirement 3: If the command option is an invalid option then your shell shall print the command followed by `: invalid option --` and the option that was invalid as well as a prompt to try —help. exec() outputs this automatically make sure you pass it on to your use

```
[bakker@omega msh_ solution]$ ./msh 
msh> ls 
hello hello.c Loop Loop.c Makefile msh msh.c msh.o         
msh> lss
lss: Command not found.

msh> ls -ijh
ls: invalid option -- j        
Try `ls --help` for more information        
msh>        

```        




























