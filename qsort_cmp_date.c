/*
 * https://www.daniweb.com/programming/software-development/threads/405653/qsort-an-array-of-structures-by-2-criteria
 *  #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    struct record {
        char name[32];
        double value;
    };
    int compare(const void *a, const void *b)
    {
        const struct record *pa = a;
        const struct record *pb = b;

        int diff = strcmp(pa->name, pb->name);

        if (diff == 0) {
            if (pa->value < pb->value)
                diff = -1;
            else if (pa->value > pb->value)
                diff = +1;
            else
                diff = 0;
        }

        return diff;
    }
    int main(void)
    {
        struct record records[] = {
            {"A", 1},
            {"B", 13},
            {"B", 1},
            {"B", 2},
            {"C", 3},
            {"C", 1}
        };
        int size = 6;
        int i;

        qsort(records, size, sizeof(*records), compare);

        for (i = 0; i < 6; i++)
            printf("%s %f\n", records[i].name, records[i].value);

        return 0;
    }

 * */

#include <stdio.h>
#include <stdlib.h>

typedef struct date {
    char file_name[256];
    int year;
    int month;
    int day;
    int hour;
    int minute;
    int second;
} Date;

int cmp_date(const void *a, const void *b) {
    const struct date *ptr_a = a;
    const struct date *ptr_b = b;

    int diff_year = ptr_a->year - ptr_b->year;

    // If it is the same year
    if (diff_year == 0) {
        // If it is not the same month
        if (ptr_a->month < ptr_b->month)
            return -1;
            // If it is not the same month
        else if (ptr_a->month > ptr_b->month)
            return 1;
            // If it is the same month
        else {
            // If it is not the same day
            if (ptr_a->day < ptr_b->day)
                return -1;
                // If it is not the same day
            else if (ptr_a->day > ptr_b->day)
                return 1;
                // If it is the same day
            else
                return 0;
        }
    }

    // If it is not the same year
    return diff_year;
}

int cmp_year_month_day(const void *a, const void *b) {
    const struct date *ptr_a = a;
    const struct date *ptr_b = b;

    int diff_year = ptr_a->year - ptr_b->year;

    // If it is not the same year
    if (diff_year != 0) {
        return diff_year;
    }

    // If it is the same year, we will compare month
    int diff_month = ptr_a->month - ptr_b->month;

    // If it is not the same month
    if (diff_month != 0) {
        return diff_month;
    }

    // If it is the same month, we will compare day
    int diff_day = ptr_a->day - ptr_b->day;

    // If it is not the same day
    if (diff_day != 0) {
        return diff_day;
    }

    // If it is the same day
    return 0;

}

int compare_date(const void *a, const void *b) {
    const struct date *ptr_a = a;
    const struct date *ptr_b = b;

    int diff_year = ptr_a->year - ptr_b->year;

    // If it is the same year, we will compare month
    if (diff_year == 0) {
        int diff_month = ptr_a->month - ptr_b->month;
        // If it is the same month, we will compare day
        if (diff_month == 0) {
            int diff_day = ptr_a->day - ptr_b->day;
            return diff_day;
        }
        // If it is not the same month
        return diff_month;
    }

    // If it is not the same year
    return diff_year;

}

int cmp_year_month_day_hour_minute_second(const void *a, const void *b) {
    const struct date *ptr_a = a;
    const struct date *ptr_b = b;

    int diff_year = ptr_a->year - ptr_b->year;

    int diff_month = ptr_a->month - ptr_b->month;

    int diff_day = ptr_a->day - ptr_b->day;

    int diff_hour = ptr_a->hour - ptr_b->hour;

    int diff_minute = ptr_a->minute - ptr_b->minute;

    int diff_second = ptr_a->second - ptr_b->second;

    if (diff_year == 0) {
        if (diff_month == 0) {
            if (diff_day == 0) {
                if (diff_hour == 0) {
                    if (diff_minute == 0) {
                        if (diff_second == 0) {
                            return 0;
                        }

                        return diff_second;
                    }

                    return diff_minute;
                }

                return diff_hour;
            }

            return diff_day;
        }

        return diff_month;
    }

    return diff_year;

}

int cmp_date_time(const void *a, const void *b) {
    const struct date *ptr_a = a;
    const struct date *ptr_b = b;

    int diff_year = 0;

    // Compare year
    if (ptr_a->year < ptr_b->year)
        diff_year = -1;
    else if (ptr_a->year > ptr_b->year)
        diff_year = 1;
        // If the years are the same
    else {
        // compare month
        if (ptr_a->month < ptr_b->month)
            diff_year = -1;
        else if (ptr_a->month > ptr_b->month)
            diff_year = 1;
            // If the months are the same
        else {
            // compare days
            if (ptr_a->day < ptr_b->day)
                diff_year = -1;
            else if (ptr_a->day > ptr_b->day)
                diff_year = 1;
                // If the days are the same
            else {
                // compare hours
                if (ptr_a->hour < ptr_b->hour)
                    diff_year = -1;
                else if (ptr_a->hour > ptr_b->hour)
                    diff_year = 1;
                    // If the hours are the same
                else {
                    // compare minutes
                    if (ptr_a->minute < ptr_b->minute)
                        diff_year = -1;
                    else if (ptr_a->minute > ptr_b->minute)
                        diff_year = 1;
                        // If the minutes are the same
                    else {
                        // compare seconds
                        if (ptr_a->second < ptr_b->second)
                            diff_year = -1;
                        else if (ptr_a->second > ptr_b->second)
                            diff_year = 1;
                        else
                            diff_year = 0;
                    }
                }
            }
        }
    }

    return diff_year;

}

//int sortDates(const void *a, const void *b) {
//    const struct date *ptr_a = a;
//    const struct date *ptr_b = b;
//
//    if (ptr_a->year < ptr_b->year)
//        return -1;
//    else if (ptr_a->year > ptr_b->year)
//        return 1;
//
//
//    /* here you are sure the years are equal, so go on comparing the months */
//
//    if (ptr_a->month < ptr_b->month)
//        return -1;
//    else if (ptr_a->month > ptr_b->month)
//        return 1;
//
//    /* here you are sure the months are equal, so go on comparing the days */
//
//    if (ptr_a->day < ptr_b->day)
//        return -1;
//    else if (ptr_a->day > ptr_b->day)
//        return 1;
//    else
//        return 0; /* definitely! */
//
//}

int main() {
    Date dates[] = {
            {"file2", 2013, 1, 1, 12, 23, 20},
            {"file6", 2012, 1, 3, 12, 19, 15},
            {"file3", 2012, 1, 2, 12, 15, 10},
            {"file5", 2012, 3, 1, 11, 20, 20},
            {"file8", 2011, 2, 2, 10, 10, 20},
            {"file9", 2011, 2, 2, 10, 10, 10}
    };
    int size = 6;
    int i;

    qsort(dates, size, sizeof(*dates), cmp_date_time);

    for (i = 0; i < 6; i++)
        printf("%s %d-%d-%d %d:%d:%d\n", dates[i].file_name, dates[i].year, dates[i].month, dates[i].day, dates[i].hour,
               dates[i].minute, dates[i].second);

    return 0;
}
