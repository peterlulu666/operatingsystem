#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <sys/mman.h>

// the earthquake data is stored in struct
typedef struct earthquake {
    char time[256];
    double latitude;
} Earthquake;

int main() {
    // check if the file is opened
    FILE *file;
    file = fopen("all_month.csv", "r");
    if (file == NULL) {
        perror("The file will not be able to opened. \n");
        exit(0);
    }

    // initialize struct
    /*This code was taken from the website
     * Source code website:
     * https://stackoverflow.com/questions/5656530/how-to-use-shared-memory-with-linux-%20in-c
     * Source code:
        #include <stdio.h>
        #include <stdlib.h>
        #include <sys/mman.h>

        void* create_shared_memory(size_t size) {
          // Our memory buffer will be readable and writable:
          int protection = PROT_READ | PROT_WRITE;

          // The buffer will be shared (meaning other processes can access it), but
          // anonymous (meaning third-party processes cannot obtain an address for it),
          // so only this process and its children will be able to use it:
          int visibility = MAP_SHARED | MAP_ANONYMOUS;

          // The remaining parameters to `mmap()` are not important for this use case,
          // but the manpage for `mmap` explains their purpose.
          return mmap(NULL, size, protection, visibility, -1, 0);
        }
        What I have learned:
        I learned the Shared memory
     * */
    int protection = PROT_READ | PROT_WRITE;
    int visibility = MAP_SHARED | MAP_ANONYMOUS;
    Earthquake *earthquakeData = (Earthquake *) mmap(NULL, INT16_MAX, protection, visibility, -1, 0);
//    Earthquake earthquakeData[INT16_MAX];
//    Earthquake *earthquakeData = (Earthquake *) malloc(INT16_MAX * sizeof(Earthquake));

    // read the data
    /*This code was taken from the website
     * Source code website:
     * https://stackoverflow.com/questions/44217917/read-specific-line-from-csv-file-in-c
     * Source code:
     *  char buff[1024];
     *  fgets(buff, 1024, fp);
     *  fgets(buff, 1024, fp);
     *  What I have learned:
     *  I learned how to read data from the second line
     * */

    char line[1024];
    char delimiter[256] = ",";
    int numberOfLine = 0;

    // read the header
    fgets(line, 1024, file);
    // read the data from the second line
    while (fgets(line, 1024, file) != NULL) {
        // use strtok() to split the string
        char *token = strtok(line, delimiter);

        // store time data to the struct
        strcpy(earthquakeData[numberOfLine].time, token);

        // use strtok() to split the string
        token = strtok(NULL, delimiter);

        // store latitude data to the struct
        // atof() will convert string to double
        earthquakeData[numberOfLine].latitude = atof(token);

        // increment numberOfLine
        numberOfLine++;
    }

    // at this point, we will start bubble sort
    clock_t start = clock();

    // sort the data in ascending order
    // bubble sort
    for (int j = 0; j < numberOfLine; ++j) {
        for (int i = 0; i < numberOfLine; ++i) {
            if (earthquakeData[i].latitude > earthquakeData[i + 1].latitude) {
                Earthquake tmp;
                tmp = earthquakeData[i];
                earthquakeData[i] = earthquakeData[i + 1];
                earthquakeData[i + 1] = tmp;
            }
        }
    }

    // closes the stream
    fclose(file);

    // at this point, we will finish bubble sort
    clock_t finish = clock();

    // in order to get the number of seconds, we will need to divide by CLOCKS_PER_SEC
    double run_time = (double) (finish - start) / CLOCKS_PER_SEC;

    // print the run time
    printf("The run time is %f seconds. \n", run_time);

//    // print the data
//    for (int i = 0; i < numberOfLine; ++i) {
//        printf("time\t\t\t\tlatitude\n");
//        printf("%s\t%f\n\n",
//               earthquakeData[i].time,
//               earthquakeData[i].latitude);
//    }
//
//    // store the data to the file
//    FILE *saveToFile = fopen("sorted.csv", "w");
//    fprintf(saveToFile, "time,latitude\n");
//    for (int k = 0; k < numberOfLine; ++k) {
//        fprintf(saveToFile, "%s,%f\n",
//                earthquakeData[k].time,
//                earthquakeData[k].latitude);
//    }
//
//    // closes the stream
//    fclose(saveToFile);

    return 0;
}

