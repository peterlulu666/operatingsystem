#include <stdio.h>
#include <stdlib.h>

typedef struct student {
    char student_name[256];
    int gpa;
    int distance;
} Student;

int cmp_gpa_distance(const void *a, const void *b) {
    const struct student *ptr_a = a;
    const struct student *ptr_b = b;

    // GPA is high - low
    int diff_gpa = ptr_b->gpa - ptr_a->gpa;

    // If it is not the same gpa
    if (diff_gpa != 0) {
        return diff_gpa;
    }

    // If it is the same gpa, we will compare distance
    // distance is low - high
    int diff_distance = ptr_a->distance - ptr_b->distance;

    // If it is not the same distance
    if (diff_distance != 0) {
        return diff_distance;
    }

    // if it is the same distance
    return 0;

}

int main() {
    Student students[] = {
            {"student1", 1, 5},
            {"student2", 2, 10},
            {"student3", 3, 20},
            {"student4", 3, 5},
            {"student5", 3, 1},
            {"student6", 4, 10}
    };

    int size = 6;
    int i;

    qsort(students, size, sizeof(*students), cmp_gpa_distance);

    for (i = 0; i < 6; i++)
        printf("%s GPA %d distance %d\n", students[i].student_name, students[i].gpa, students[i].distance);

    return 0;
}
