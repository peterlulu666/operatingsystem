#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main() {
    while (1) {
        char prompt[20];
        printf("Enter name: ");
        scanf("%s", prompt);
        pid_t pid = fork();
        if (pid == 0) {

            char *arguments[4];

            arguments[0] = (char *) malloc(strlen(prompt));

            strncpy(arguments[0], prompt, strlen(prompt));

            arguments[2] = NULL;

            // Notice you can add as many NULLs on the end as you want
            int ret = execvp(arguments[0], &arguments[0]);
            if (ret == -1) {
                printf("%s: Command not found.\n", arguments[0]);
            }
        } else {
            int status;
            wait(&status);
        }

        return 0;
    }

}


//int main() {
//    char *ls_args[] = {"/bin/ls", "-l", NULL};
//    execv(ls_args[0], ls_args);
//    perror("execv");
//    return 0;
//
//}



//int main() {
//    char *ls_args[] = {"ls", "-a", NULL};
//    // The execvp will look for ls PATH
//    execvp(ls_args[0], ls_args);
//    // execvp(ls_args[0], &ls_args[0]);
//    perror("execv");
//    return 0;
//}

